$(document).ready(function(){
    $('body').on('click', '.more', function() {
        var data = json('/sub/reviewList/'+($(this).attr('data-now')*1+1)+'?word='+$('#searchKeyword').val());
        var html = '';
        if(data.return){
          for(var i=0; i< data.list.length; i++) {
            html += '<li><div class="list-img"><a href="/sub/review_view/'+data.list[i].example_idx+'"><div style="background-image:url(\'/assets/uploads\/'+data.list[i].example_thumb+'\');"></div></a></div><div class="list-contents"><div class="title"><a href="/sub/review_view/'+data.list[i].example_idx+'">'+data.list[i].example_title+'</a></div><p class="text" style=" width: 330px;   overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 3; -webkit-box-orient: vertical;">'+data.list[i].example_text.replace(/(<([^>]+)>)/ig,"")+'</p><div class="hr"></div><div class="list-info"><div class="writer"><img src="/assets/images/sample.png" class="img-circle info-logo"><span>라돈닥터</span></div><div class="sns"><a class="like" data-idx="'+data.list[i].example_idx+'"></a><span>'+data.list[i].example_like+'</span><a href="#" class="reply"></a><span>'+data.list[i].replycount+'</span></div></div></div></li>';
          }
          $('.cbp-list').append(html);
          var now = $('.more').attr('data-now')*1 + 1;
          var all = $('.more').attr('data-all')*1;
          if(now == all) {
            $('.more').remove();
          }
          else {
            $('.more').attr('data-now',now);
          }
        }
    });

    $('body').on('click', '.morenews', function() {
        var data = json('/sub/newsList/'+($(this).attr('data-now')*1+1)+'?word='+$('#searchKeyword').val());
        var html = '';
        if(data.return){
          for(var i=0; i< data.list.length; i++) {
            html += '<li><div class="list-img"><a href="/sub/news_view/'+data.list[i].news_idx+'"><div style="background-image:url(\'/assets/uploads\/'+data.list[i].news_thumb+'\');"></div></a></div><div class="list-contents"><div class="title"><a href="/sub/news_view/'+data.list[i].news_idx+'">'+data.list[i].news_title+'</a></div><p class="text" style=" width: 330px;   overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 3; -webkit-box-orient: vertical;">'+data.list[i].news_text.replace(/(<([^>]+)>)/ig,"")+'</p><div class="hr"></div><div class="list-info"><div class="writer"><img src="/assets/images/sample.png" class="img-circle info-logo"><span>라돈닥터</span></div><div class="sns"><a class="like" data-idx="'+data.list[i].news_idx+'"></a><span>'+data.list[i].news_like+'</span><a href="#" class="reply"></a><span>'+data.list[i].replycount+'</span></div></div></div></li>';
          }
          $('.cbp-list').append(html);
          var now = $('.more').attr('data-now')*1 + 1;
          var all = $('.more').attr('data-all')*1;
          if(now == all) {
            $('.morenews').remove();
          }
          else {
            $('.morenews').attr('data-now',now);
          }
        }
    });

    $('body').on('click', '.like, .newslike', function() {
      var idx = $(this).attr('data-idx');
        if($(this).hasClass('newslike')) {
          var data = json('/sub/news_like/'+idx);
        } else {
          var data = json('/sub/review_like/'+idx);
        }
        $(this).addClass('on');
        var like = data.like;
        $(this).next().text(like);
    });

    $('body').on('click', '.locationbtn', function() {
      if($(this).attr('data-idx')){
        location.href = "http://radon.com/sub/review_view/"+$(this).attr('data-idx');
      }
    });

    $('body').on('click', '.locationbtns', function() {
      if($(this).attr('data-idx')){
        location.href = "http://eunjinn05.cafe24.com/sub/notice_view/"+$(this).attr('data-idx');
      }
    });

    $('body').on('click', '#show_reply', function() {
      if($(".board_reply_form").css("display") == "none"){
        $(".board_reply_form").show();
      } else {
        $(".board_reply_form").hide();
      }
    });

    $('body').on('click', '#replygo, #replyupdate', function() {
      var check = true;
       $('.req').each(function() {
         if (!$(this).val()) {
             check = false;
             return false;
         }
      });
      if(check){
        var form = $('form')[0];
        var data = new FormData(form);
        if($(this).attr('id') == "replygo"){ var url = "/sub/insert_reply"; var text = "등록되었습니다";}
        else { var url = "/sub/reply_update/"+$('#reply_idx').val(); var text = "수정되었습니다"; }
        var jsons = jsonreturn(url,data);
        if(jsons.return == true) {
          alert(text);
          location.reload();
        }
      } else {
        $(this).next().trigger('click');
      }
    });

    $('body').on('click', '#noreplygo, #noreplyupdate', function() {
      var check = true;
       $('.req').each(function() {
         if (!$(this).val()) {
             check = false;
             return false;
         }
      });
      if(check){
        var form = $('form')[0];
        var data = new FormData(form);
        if($(this).attr('id') == "noreplygo"){ var url = "/sub/insert_noreply"; var text = "등록되었습니다";}
        else { var url = "/sub/noreply_update/"+$('#noreply_idx').val(); var text = "수정되었습니다"; }
        var jsons = jsonreturn(url,data);
        if(jsons.return == true) {
          alert(text);
          location.reload();
        }
      } else {
        $(this).next().trigger('click');
      }
    });


    $('body').on('click', '#newsreplygo', function() {
      var check = true;
       $('.req').each(function() {
         if (!$(this).val()) {
             check = false;
             return false;
         }
      });
      if(check){
        var form = $('form')[0];
        var data = new FormData(form);
        if($(this).attr('id') == "newsreplygo"){ var url = "/sub/insert_newsreply"; var text = "등록되었습니다";}
        else { var url = "/sub/noreply_update/"+$('#noreply_idx').val(); var text = "수정되었습니다"; }
        var jsons = jsonreturn(url,data);
        if(jsons.return == true) {
          alert(text);
          location.reload();
        }
      } else {
        $(this).next().trigger('click');
      }
    });


  $('body').on('click', '.replyidx, .noreplyidx, .newsreplyidx', function() {
     $('.board_reply_form').css('display','block');
      var replyidx = $(this).parent().prev().attr('data-idx');
      $('#replyidx').val(replyidx);
  });
  $('body').on('click', '.replyedit, .noreplyedit, .newsreplyedit', function() {
      var con = prompt("비밀번호를 입력해주세요");
      var text = $(this).parent().parent().next().children().first().text();
      var idx = $(this).parent().parent().attr('data-idx');
      if($(this).hasClass('replyedit')) {
        var data = json('/sub/reply_data/'+idx);
        var classs = "replyrebutton";
      }
      else if($(this).hasClass('noreplyedit')) {
        var data = json('/sub/noreply_data/'+idx);
        var classs = "noreplyrebutton";
      }
      else {
        var data = json('/sub/newsreply_data/'+idx);
        var classs = "newsreplyrebutton";
      }
        if(con == data.pass){
          $(this).parent().parent().next().html('<textarea cols="30" id="reply_text" class="req" rows="3" placeholder="답글쓰기" required="required">'+text+'</textarea> <button type="button" class="'+classs+'" data-idx='+idx+'>수정하기</button>');
        } else {
          alert("비밀번호를 확인해주세요");
        }
  });
  $('body').on('click', '.replyrebutton, .noreplyrebutton, .newsreplyrebutton', function() {
    if($(this).hasClass('replyrebutton')){
      var data = {'rereply_text':$(this).prev().val()};
      var jsons = json('/sub/reply_update/'+$(this).attr('data-idx'),data);
    }
    else if($(this).hasClass('noreplybutton')) {
      var data = {'noreply_text':$(this).prev().val()};
      var jsons = json('/sub/noreply_update/'+$(this).attr('data-idx'),data);
    }
    else {
      var data = {'newsreply_text':$(this).prev().val()};
      var jsons = json('/sub/newsreply_update/'+$(this).attr('data-idx'),data);
    }
      location.reload();
  });

  $('body').on('click', '.replydel, .noreplydel, .newsreplydel', function() {
      var con = prompt("비밀번호를 입력해주세요");
      var idx = $(this).parent().parent().attr('data-idx');
      if($(this).hasClass('replydel')) {
        var data = json('/sub/reply_data/'+idx);
        if(con == data.pass){
          var data = json('/sub/reply_del/'+idx);
        } else{
          alert("비밀번호를 확인해주세요");
        }
       }
       else if($(this).hasClass('noreplydel')) {
         var data = json('/sub/noreply_data/'+idx);
         if(con == data.pass){
           var data = json('/sub/noreply_del/'+idx);
         } else{
           alert("비밀번호를 확인해주세요");
         }
       }
       else {
         var data = json('/sub/newsreply_data/'+idx);
         if(con == data.pass){
           var data = json('/sub/newsreply_del/'+idx);
         } else{
           alert("비밀번호를 확인해주세요");
         }
       }
       if(data.return) {
         location.reload();
       }

      // if(con) {
      //   var idx = $(this).parent().parent().attr('data-idx');
      //   if($(this).hasClass('replydel')) {
      //     var data = json('/sub/reply_del/'+idx);
      //   }
      //   else {
      //     var data = json('/sub/noreply_del/'+idx);
      //   }
      //   if(data.return) {
      //     location.reload();
      //   }
      // }
  });

  $('body').on('click', '#noticego', function() {
      var word = $('#word').val();
      var type = $('#select').val();
      if(word) {
        location.href='http://radon.com/sub/notice/1?type='+type+'&word='+word;
      }
  });

  $('body').on('click', '#search', function() {
      var word = $('#searchKeyword').val();
      location.href = 'http://radon.com/sub/review/1?word='+word;
  });

  $('body').on('click', '#newssearch', function() {
      var word = $('#newsword').val();
      location.href = 'http://radon.com/sub/news/1?word='+word;
  });

  // qna ---------------------------------------------------
  $('body').on('click', '#qnago', function() {
    var check = true;
     $('.req').each(function() {
       if (!$(this).val()) {
           check = false;
           return false;
       }
    });
    if(check){
      var form = $('form')[0];
      var data = new FormData(form);
      if($(this).attr('id') == "qnago"){ var url = "/sub/insert_qna"; var text = "등록되었습니다";}
      else { var url = "/sub/reply_update/"+$('#reply_idx').val(); var text = "수정되었습니다"; }
      var jsons = jsonreturn(url,data);
      if(jsons.return == true) {
        alert(text);
        location.reload();
      }
    } else {
      $(this).next().trigger('click');
    }
  });
});
