$(document).ready(function(){
    jsonreturn = function(url,data) {
        var option = {
            url : 'http://radon.com'+url,
            async:false
        };
        if(data){
            option.data = data;
            option.type = "post";
            option.contentType = false;
            option.processData = false
        }
        $.ajax(
            option
        ).done(function(data){
            result = data;
        });
        return result;
    }
    json = function(url,data) {
        var option = {
            url : 'http://radon.com'+url,
            data : data,
            type : "post",
            async:false
        };
        $.ajax(
            option
        ).done(function(data){
            result = data;
        });
        return result;
    }

    uploadImage = function(form) {
        var image = new FormData();
        for (var I = 0; I < form[0].files.length; I++) {
            image.append('images[]', form[0].files[I]);
        }
        if (form.data('width')) {
            image.append('x', form.data('width'));
            if (form.data('height')) {
                image.append('y', form.data('height'));
            }
        }
        return this.jsonreturn('/admin/uploadimage', image);
    }

    emailsend = function() {
        var email = $('#email').val();
        var title = '제휴문의사항이 도착했습니다';
        var text = '문의사항 \n이름 : '+$('#name').val()+'\n이메일 : '+$('#email').val()+'\n연락처 : '+$('#phone').val()+'\n 문의사항 :'+$('#question').val();
        var result = json('/admin/sendmail/',{'title':title,'text':text,'email':email});
        if(result.result == true) {
            alert("메일이 전송되었습니다");
        }
    }
  

});
