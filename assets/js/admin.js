$(document).ready(function(){
     function admin() {
         this.example = function() {
             $('body').on('click', '#reviewgo, #reviewedit', function() {
               oEditors.getById["review_text"].exec("UPDATE_CONTENTS_FIELD", []);
               if($(this).attr('id') == "reviewgo") {
                 var url = "/admin/reviewInsert";
                 var text = "등록되었습니다";
               }
               else {
                 var url = "/admin/reviewUpdate/"+$('#idx').val();
                 var text = "수정되었습니다";
               }
                 var check = true;
                 $('.req').each(function() {
                     if (!$(this).val()) {
                          check = false;
                          return false;
                     }
                 });
                 if(check) {
                     var form = $('form')[0];
                     var data = new FormData(form);
                     var json = jsonreturn(url,data);
                     if(json.return == true) {
                         location.href = "/admin/reviewList/1";
                     }
                 }
                 else {
                     $(this).next().trigger('click');
                 }
             });

             $('body').on('click', '.adminedit', function() {
               var link = $(this).attr('data-url');
               var idx = $(this).parent().parent().attr('data-idx');
               location.href=link+'/'+idx;
            });
            $('body').on('click', '.adminview', function() {
              var link = $(this).attr('data-url');
              var idx = $(this).parent().parent().attr('data-idx');
              location.href=link+'/'+idx;
           });

            $('body').on('click', '.admindel', function() {
                var con = confirm("정말 삭제하시겠습니까?");
                if(con) {
                  var idx = $(this).parent().parent().attr('data-idx');
                  var url = $(this).attr('data-url');
                  var result = jsonreturn(url+'/'+idx);
                  if(result.return == true) {
                    location.reload();
                  }
                }
           });

           //이미지 1개
           // $('body').on('change', '#image', function() {
           //    var json = uploadImage($(this));
           //    if(json.url) {
           //      $(this).next().next().attr('src','http://sample.com/assets/uploads/'+json.url);
           //      $(this).next().next().css('display','block');
           //      $(this).next().val(json.url);
           //    }
           //  });

           //이미지 여러개
           $('body').on('change', '#image', function() {
              var json = uploadImage($(this));
              if(json.url) {
                for(var i = 0; i < json.url.length; i++) {
                  $(this).parent().append('<img src="http://radon.com/assets/uploads/'+json.url[i]+'" class="moreimages">');
                  $(this).parent().append('<input type="hidden" value="'+json.url[i]+'" name="image[]" class="req">');
                }
              }
            });

            $('body').on('click', '.moreimages', function() {
              $(this).next().remove();
              $(this).remove();
            });
         }

         this.notice = function() {
             $('body').on('click', '#noticego, #noticeedit', function() {
               oEditors.getById["notice_text"].exec("UPDATE_CONTENTS_FIELD", []);
               if($(this).attr('id') == "noticego") {
                 var url = "/admin/noticeInsert";
                 var text = "등록되었습니다";
               }
               else {
                 var url = "/admin/noticeUpdate/"+$('#idx').val();
                 var text = "수정되었습니다";
               }
                 var check = true;
                 $('.req').each(function() {
                     if (!$(this).val()) {
                          check = false;
                          return false;
                     }
                 });
                 if(check) {
                     var form = $('form')[0];
                     var data = new FormData(form);
                     var json = jsonreturn(url,data);
                     if(json.return == true) {
                         location.href = "/admin/noticeList/1";
                     }
                 }
                 else {
                     $(this).next().trigger('click');
                 }
             });
         }
         this.expert = function() {
             $('body').on('click', '#expertgo, #expertedit', function() {
               oEditors.getById["expert_text"].exec("UPDATE_CONTENTS_FIELD", []);
               if($(this).attr('id') == "expertgo") {
                 var url = "/admin/expertInsert";
                 var text = "등록되었습니다";
               }
               else {
                 var url = "/admin/expertUpdate/"+$('#idx').val();
                 var text = "수정되었습니다";
               }
                 var check = true;
                 $('.req').each(function() {
                     if (!$(this).val()) {
                          check = false;
                          return false;
                     }
                 });
                 if(check) {
                     var form = $('form')[0];
                     var data = new FormData(form);
                     var json = jsonreturn(url,data);
                     if(json.return == true) {
                         location.href = "/admin/expertList/1";
                     }
                 }
                 else {
                     $(this).next().trigger('click');
                 }
             });
         }
         this.faq = function() {
           $('body').on('click', '#faqgo, #faqedit', function() {
             oEditors.getById["faq_text"].exec("UPDATE_CONTENTS_FIELD", []);
             if($(this).attr('id') == "faqgo") {
               var url = "/admin/faqInsert";
               var text = "등록되었습니다";
             }
             else {
               var url = "/admin/faqUpdate/"+$('#idx').val();
               var text = "수정되었습니다";
             }
               var check = true;
               $('.req').each(function() {
                   if (!$(this).val()) {
                        check = false;
                        return false;
                   }
               });
               if(check) {
                   var form = $('form')[0];
                   var data = new FormData(form);
                   var json = jsonreturn(url,data);
                   if(json.return == true) {
                       location.href = "/admin/faqList/1";
                   }
               }
               else {
                   $(this).next().trigger('click');
               }
           });
         }
         this.news = function() {
           $('body').on('click', '#newsgo, #newsedit', function() {
             oEditors.getById["news_text"].exec("UPDATE_CONTENTS_FIELD", []);
             if($(this).attr('id') == "newsgo") {
               var url = "/admin/newsInsert";
               var text = "등록되었습니다";
             }
             else {
               var url = "/admin/newsUpdate/"+$('#idx').val();
               var text = "수정되었습니다";
             }
               var check = true;
               $('.req').each(function() {
                   if (!$(this).val()) {
                        check = false;
                        return false;
                   }
               });
               if(check) {
                   var form = $('form')[0];
                   var data = new FormData(form);
                   var json = jsonreturn(url,data);
                   if(json.return == true) {
                       location.href = "/admin/newsList/";
                   }
               }
               else {
                   $(this).next().trigger('click');
               }
           });
         }
         this.qna = function() {
           $('body').on('click', '#qnago, #qnaedit', function() {
             if($(this).attr('id') == "qnago") {
               var url = "/admin/qnaInsert";
               var text = "등록되었습니다";
             }
             else {
               var url = "/admin/qnaUpdate/"+$('#idx').val();
               var text = "수정되었습니다";
             }
             var data = {
               qnaAnswer_text : $('#qnaAnswer_text').val(),
               idx : $('#idx').val()
             };
             var jsons = json(url,data);
             if(jsons.return == true) {
                 // location.href = "/admin/qnaList/";
                 alert(text);
             }
           });
         }
         this.login = function() {
           $('body').on('click', '#logingo', function() {
               var req = true;
               $('.req').each(function() {
                   if (!$(this).val()) {
                     req = false;
                     return false;
                   }
                });

               if(req) {
                   var form = $('form')[0];
                   var data = new FormData(form);
                   var json = jsonreturn('/admin/loginData',data);
                   if(json.return == true) {
                       // alert("로그인 되었습니다");
                       location.href = '/admin/';
                   }
                   else {
                       alert("아이디 비밀번호가 일치하지 않습니다");
                   }
               }
               else {
                   $(this).next().trigger('click');
               }
           });

           $(".log").keypress(function(e) {
             if (e.keyCode == 13){
               var req = true;
               $('.req').each(function() {
                   if (!$(this).val()) {
                     req = false;
                     return false;
                   }
                });

               if(req) {
                   var form = $('form')[0];
                   var data = new FormData(form);
                   var json = jsonreturn('/admin/loginData',data);
                   if(json.return == true) {
                       // alert("로그인 되었습니다");
                       location.href = '/admin/';
                   }
                   else {
                       alert("아이디 비밀번호가 일치하지 않습니다");
                   }
               }
               else {
                   $(this).next().trigger('click');
               }
             }
           });
         }
         editor = function(eachtext) {
           $('#imagesUpload').change(function() {
              var json = uploadImage($(this));
              if (json) {
                  if (json.return == 'true') {
                      for (var variable in json.url) {
                          if (json.url.hasOwnProperty(variable)) {
                              if ($('textarea+iframe').length) {
                                  var sHTML = "<img style='max-width:100%' src='/assets/uploads/"+json.url[variable]+"' />";
                                  oEditors.getById[eachtext].exec("PASTE_HTML", [sHTML]);
                              }
                          }
                      }
                      $(this).val('');
                  }
              } else {
                  alert('서버 에러');
              }
          });
        }
    }

    var admin = new admin();
    admin.faq();
    admin.expert();
    admin.example();
    admin.news();
    admin.notice();
    admin.qna();
    admin.login();
});
