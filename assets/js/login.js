$(document).ready(function(){
    function login() {
        this.join = function() {
            var pass = false;
            var ckk = false;
            if(localStorage.getItem('sns')){
                $('.snss').remove();
                $('#idx').val(localStorage.getItem('idx'));
                pass = true;
                ckk = true;
            }
            $('body').on('click', '#address', function() {
                $this = $(this);
                new daum.Postcode({
                    oncomplete: function(data) {
                        $this.next().val(data.postcode);
                        $this.val(data.address);
                    }
                }).open();
            });
            $('body').on('keyup', '#id', function() {
                var id = $(this).val();
                var result = jsonreturn('/login/ckk',{id:id});
                $('.ckk').removeClass('hidden');
                if(result == true){
                    $('.ckk').addClass('red').removeClass('green').text('중복된 아이디입니다');
                    ckk = false;
                }
                else{
                    $('.ckk').addClass('green').removeClass('red').text('중복되지 않은 아이디입니다');
                    ckk = true;
                }
            });
            $('body').on('keyup', '#pass, #passck', function() {
                $('.passck').removeClass('hidden');
                if($('#pass').val() == $('#passck').val()){
                    $('.passck').text("비밀번호가 일치합니다").removeClass('red').addClass('green');
                    pass = true;
                }
                else{
                    $('.passck').text("비밀번호가 일치하지 않습니다").removeClass('green').addClass('red');
                    pass = false;
                }
            });

            $('body').on('click', '#joingo', function() {
                var check = true;
                $('.req').each(function() {
                    if (!$(this).val()) {
                        check = false;
                        return false;
                    }
                });
                if(check){
                    if(!ckk){
                        alert("아이디를 확인해주세요");
                        return false;
                    }
                    else if(!pass){
                        alert("비밀번호를 확인해주세요");
                        return false;
                    }
                    else {
                        var form = $('form')[0];
                        var data = new FormData(form);
                        var result = jsonreturn('/login/joindata',data);
                    }
                }
                else{
                    if($('.postcode').val() == ""){
                        alert("주소를 입력하세요");
                        return false;
                    }
                    else{
                        $(this).next().trigger('click');
                        return false;
                    }
                }
            });

        }
        this.login = function() {
          $('body').on('click', '#login', function() {
              var check = true;
              $('.req').each(function() {
                  if (!$(this).val()) {
                      check = false;
                      return false;
                  }
              });
              if(check){
                  var data = new FormData($('form')[0]);
                  var result = jsonreturn('/login/logindata',data);
                  // if(result.result == true){
                  //     location.href = "/index.php/main";
                  // }
                  // else{
                  //     alert("확인해주세요");
                  // }
              }
              else{
                  $(this).next().trigger('click');
              }
          });
        }
        this.메서드2 = function() {

        }
        this.kakao = function() {
            // Kakao.init('8c4d600340b795d45b4413b1b766d471');
            // Kakao.Auth.createLoginButton({
            //     container: '#kakao-login-btn',
            //     success: function(authObj) {
            //         Kakao.API.request({
            //             url: '/v2/user/me',
            //             success: function(res) {
            //                 var result = json('/login/kakao',res);
            //                 if(result.result == true){
            //                     localStorage.setItem('sns', true);
            //                     localStorage.setItem('idx',result.idx);
            //                     location.href = '/index.php/login/join';
            //                 }
            //                 else{
            //
            //                 }
            //             },
            //             fail: function(error) {
            //                 alert(JSON.stringify(error));
            //             }
            //         });
            //     },
            //     fail: function(err) {
            //         alert(JSON.stringify(err));
            //     }
            // });
        }
    }

    var login = new login();
    login.kakao();
    login.join();
    login.login();
});
