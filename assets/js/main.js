(function($){

	/* Preloader */
	$(window).load(function() {
		$('#status').fadeOut();
		$('#preloader').delay(300).fadeOut('slow');
	});

	$(document).ready(function() {
		new fullScroll({
			mainElement: 'main',
			displayDots: true,
			dotsPosition: 'right',
			animateTime: 0.7,
			animateFunction: 'ease'
		});
	});

})(jQuery);