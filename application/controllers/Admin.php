<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model('adminmodel');
        $this->load->library('pagination');
    }

  // 메인 -----------------------------------
	public function index()
	{
      $this->nologin();
      $name = "저감사례 리스트";
      $insert = "/admin/exampleWrite";
      $col = ['번호','제목','글쓴이','좋아요','등록일','수정','삭제'];
      $col_url = ['/admin/exampleEdit','/admin/exampleDel'];
      $button = $this->adminButton($col,$col_url);

      $json = $this->adminmodel->reviewListData($idx);
      // $url = URL.'admin/exampleList/';
      // $this->paging($json, $url);
      $data = array('name'=>$name,'col'=>$col,'adminbutton'=>$button,'insert'=>$insert,'list'=>$json['list']);

      $this->load->view('admin/admin_header');
      $this->load->view('admin/example_list',$data);
	}
 // -----------------------------------------

 // 로그인 로그아웃------------------------------------
  public function login()
  {
      $this->load->view('admin/admin_login');
  }

  public function insert_login() {
      $json = $this->adminmodel->insert_login();
      $this->json($json);
  }

  public function loginData() {
      $json = $this->adminmodel->loginData();
      $this->json($json);
  }

  public function logout() {
      session_destroy();
      header("location: ".URL.'admin/login');
  }
 //-------------------------------------------

//리스트 -------------------------------------
  public function reviewList($idx = 1)
  {
      $this->nologin();
      $name = "저감사례 리스트";
      $insert = "/admin/reviewWrite";
      $col = ['번호','제목','글쓴이','좋아요','등록일','수정','삭제'];
      $col_url = ['/admin/reviewEdit','/admin/reviewDel'];
      $button = $this->adminButton($col,$col_url);

      $json = $this->adminmodel->reviewListData($idx);
      // $url = URL.'admin/exampleList/';
      // $this->paging($json, $url);
      $data = array('name'=>$name,'col'=>$col,'adminbutton'=>$button,'insert'=>$insert,'list'=>$json['list']);

      $this->load->view('admin/admin_header');
      $this->load->view('admin/review_list',$data);
  }
  public function reviewListData() {
      $json = $this->adminmodel->reviewListData();
      $this->json($json);
  }

  public function sampleNewList($idx = 1)
  {
      $this->nologin();
      $name = "샘플 뉴 리스트";
      $insert = "/admin/sampleWrite";
      $col = ['번호','이미지 이름','이미지','등록일','수정','삭제'];
      // $col_url = ['/sample/editsample','/sample/delsample'];
      $button = $this->adminButton($col);

      $json = $this->adminmodel->sampleListData($idx);
      $url = URL.'admin/sampleList/';
      $this->paging($json, $url);
      $data = array('name'=>$name,'col'=>$col,'adminbutton'=>$button,'insert'=>$insert,'list'=>$json['list']);

      $this->load->view('admin/admin_header');
      $this->load->view('admin/admin_newlist',$data);
      $this->load->view('admin/admin_footer');
  }

  public function reviewWrite()
  {
      $name = "저감사례 등록";
      $array = array('name'=>$name);
      $this->load->view('admin/admin_header');
      $this->load->view('admin/review_insert',$array);
  }

    public function reviewInsert() {
        $json = $this->adminmodel->reviewInsert();
        $this->json($json);
    }

    public function reviewEdit($idx)
    {
        $name = "저감사례 수정";
        $this->load->view('admin/admin_header');
        $json = $this->adminmodel->reviewData($idx);
        $array = array('name'=>$name,'data'=>$json);
        $this->load->view('admin/review_insert',$array);
    }

    public function reviewUpdate($idx) {
        $json = $this->adminmodel->reviewUpdate($idx);
        $this->json($json);
    }

    public function reviewDel($idx) {
        $json = $this->adminmodel->reviewDel($idx);
        $this->json($json);
    }

//------------------------------------------

//공지사항 -------------------------------------
  public function noticeList($idx = 1)
  {
      $this->nologin();
      $name = "공지사항 리스트";
      $insert = "/admin/noticeWrite";
      $col = ['번호','제목','글쓴이','좋아요','등록일','수정','삭제'];
      $col_url = ['/admin/noticeEdit','/admin/noticeDel'];
      $button = $this->adminButton($col,$col_url);

      $json = $this->adminmodel->noticeListData($idx);
      // $url = URL.'admin/exampleList/';
      // $this->paging($json, $url);
      $data = array('name'=>$name,'col'=>$col,'adminbutton'=>$button,'insert'=>$insert,'list'=>$json['list']);

      $this->load->view('admin/admin_header');
      $this->load->view('admin/notice_list',$data);
  }

    public function noticeWrite()
    {
        $name = "공지사항 등록";
        $array = array('name'=>$name);
        $this->load->view('admin/admin_header');
        $this->load->view('admin/notice_insert',$array);
    }

    public function noticeInsert() {
        $json = $this->adminmodel->noticeInsert();
        $this->json($json);
    }

    public function noticeData($idx) {
        $json = $this->adminmodel->noticeData($idx);
        $this->json($json);
    }

    public function noticeEdit($idx)
    {
        $name = "공지사항 수정";
        $this->load->view('admin/admin_header');
        $json = $this->adminmodel->noticeData($idx);
        $array = array('name'=>$name,'data'=>$json);
        $this->load->view('admin/notice_insert',$array);
    }

    public function noticeUpdate($idx) {
        $json = $this->adminmodel->noticeUpdate($idx);
        $this->json($json);
    }

    public function noticeDel($idx) {
        $json = $this->adminmodel->noticeDel($idx);
        $this->json($json);
    }
//-------------------------------------------
//전문가 소식------------------------------------------
public function expertList($idx = 1)
{
    $this->nologin();
    $name = "전문가소식 리스트";
    $insert = "/admin/expertWrite";
    $col = ['번호','제목','글쓴이','좋아요','등록일','수정','삭제'];
    $col_url = ['/admin/expertEdit','/admin/expertDel'];
    $button = $this->adminButton($col,$col_url);

    $json = $this->adminmodel->expertListData($idx);
    // $url = URL.'admin/exampleList/';
    // $this->paging($json, $url);
    $data = array('name'=>$name,'col'=>$col,'adminbutton'=>$button,'insert'=>$insert,'list'=>$json['list']);

    $this->load->view('admin/admin_header');
    $this->load->view('admin/expert_list',$data);
}

  public function expertListData($idx) {
      $json = $this->adminmodel->expertListData($idx);
      $this->json($json);
  }

  public function expertWrite()
  {
      $name = "전문가소식 등록";
      $array = array('name'=>$name);
      $this->load->view('admin/admin_header');
      $this->load->view('admin/expert_insert',$array);
  }

  public function expertInsert() {
      $json = $this->adminmodel->expertInsert();
      $this->json($json);
  }

  public function expertEdit($idx)
  {
      $name = "전문가소식 수정";
      $this->load->view('admin/admin_header');
      $json = $this->adminmodel->expertData($idx);
      $array = array('name'=>$name,'data'=>$json);
      $this->load->view('admin/expert_insert',$array);
  }

    public function expertUpdate($idx) {
        $json = $this->adminmodel->expertUpdate($idx);
        $this->json($json);
    }
    public function expertDel($idx) {
        $json = $this->adminmodel->expertDel($idx);
        $this->json($json);
    }

//------------------------------------------------------
//faq---------------------------------------------------
  public function faqList($idx = 1)
  {
      $this->nologin();
      $name = "FAQ 리스트";
      $insert = "/admin/faqWrite";
      $col = ['번호','제목','글쓴이','좋아요','등록일','수정','삭제'];
      $col_url = ['/admin/faqEdit','/admin/faqDel'];
      $button = $this->adminButton($col,$col_url);

      $json = $this->adminmodel->faqListData($idx);
      // $url = URL.'admin/exampleList/';
      // $this->paging($json, $url);
      $data = array('name'=>$name,'col'=>$col,'adminbutton'=>$button,'insert'=>$insert,'list'=>$json['list']);

      $this->load->view('admin/admin_header');
      $this->load->view('admin/faq_list',$data);
  }

  public function faqWrite()
  {
      $name = "FAQ 등록";
      $array = array('name'=>$name);
      $this->load->view('admin/admin_header');
      $this->load->view('admin/faq_insert',$array);
  }

  public function faqInsert() {
      $json = $this->adminmodel->faqInsert();
      $this->json($json);
  }

  public function faqEdit($idx)
  {
      $name = "FAQ 수정";
      $this->load->view('admin/admin_header');
      $json = $this->adminmodel->faqData($idx);
      $array = array('name'=>$name,'data'=>$json);
      $this->load->view('admin/faq_insert',$array);
  }

  public function faqUpdate($idx) {
      $json = $this->adminmodel->faqUpdate($idx);
      $this->json($json);
  }

  public function faqDel($idx) {
      $json = $this->adminmodel->faqDel($idx);
      $this->json($json);
  }
//-----------------------------------------------------

  public function mail() {
      $this->load->view('mail');
  }

  public function sendmail() {
      $json = $this->email();
      $this->json($json);
  }

  public function uploadimage() {
      $json = $this->upload('image');
      return $this->json($json);
  }

  public function uploadfiles() {
      $json = $this->upload('file');
      return $this->json($json);
  }
  // news ----------------------------------------------
  public function newsList($idx = 1)
  {
      $this->nologin();
      $name = "News 리스트";
      $insert = "/admin/newsWrite";
      $col = ['번호','제목','글쓴이','좋아요','등록일','수정','삭제'];
      $col_url = ['/admin/newsEdit','/admin/newsDel'];
      $button = $this->adminButton($col,$col_url);

      $json = $this->adminmodel->newsListData($idx);
      // $url = URL.'admin/exampleList/';
      // $this->paging($json, $url);
      $data = array('name'=>$name,'col'=>$col,'adminbutton'=>$button,'insert'=>$insert,'list'=>$json['list']);

      $this->load->view('admin/admin_header');
      $this->load->view('admin/news_list',$data);
  }
  public function newsListData() {
      $json = $this->adminmodel->newsListData();
      $this->json($json);
  }

  public function newsWrite()
  {
      $name = "News 등록";
      $array = array('name'=>$name);
      $this->load->view('admin/admin_header');
      $this->load->view('admin/news_insert',$array);
  }
  public function newsInsert() {
      $json = $this->adminmodel->newsInsert();
      $this->json($json);
  }

  public function newsEdit($idx)
  {
      $name = "News 수정";
      $this->load->view('admin/admin_header');
      $json = $this->adminmodel->newsData($idx);
      $array = array('name'=>$name,'data'=>$json);
      $this->load->view('admin/news_insert',$array);
  }
  public function newsData($idx) {
      $json = $this->adminmodel->newsData($idx);
      $this->json($json);
  }
  public function newsUpdate($idx) {
      $json = $this->adminmodel->newsUpdate($idx);
      $this->json($json);
  }
  public function newsDel($idx) {
      $json = $this->adminmodel->newsDel($idx);
      $this->json($json);
  }
  //qna----------------------------------------------------------
  public function qnaList($idx = 1)
  {
      $this->nologin();
      $name = "QnA 리스트";
      $col = ['번호','제목','글쓴이','날짜','보기','삭제'];
      $col_url = ['/admin/qnaView','/admin/qnaDel'];
      $button = $this->adminButton($col,$col_url);

      $json = $this->adminmodel->qnaListData($idx);
      // $url = URL.'admin/exampleList/';
      // $this->paging($json, $url);
      $data = array('name'=>$name,'col'=>$col,'adminbutton'=>$button,'insert'=>$insert,'list'=>$json['list']);

      $this->load->view('admin/admin_header');
      $this->load->view('admin/qna_list',$data);
  }

  public function qnaListData() {
      $json = $this->adminmodel->qnaListData();
      $this->json($json);
  }

  public function qnaView($idx) {
      $name = "QnA 답변";
      $json = $this->adminmodel->qnaData($idx);
      $this->load->view('admin/admin_header');
      $this->load->view('admin/qna_view',array('data'=>$json));
  }

  public function qnaData($idx) {
      $json = $this->adminmodel->qnaData($idx);
      $this->json($json);
  }

  public function qnaInsert() {
      $json = $this->adminmodel->qnaInsert();
      $this->json($json);
  }

  public function qnaUpdate($idx) {
      $json = $this->adminmodel->qnaUpdate($idx);
      $this->json($json);
  }
  public function qnaDel($idx) {
      $json = $this->adminmodel->qnaDel($idx);
      $this->json($json);
  }
}
