<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sub extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model('submodel');
        $this->load->library('pagination');
    }

//---------------------------라돈 ------------------------------

  public function index()
  {
      $json = $this->submodel->faqList();
      $this->load->view('index',array('list'=>$json));
  }

   public function faqList()
   {
       $json = $this->submodel->faqList();
       $this->json($json);
   }

  public function radondr()
  {
      $this->load->view('radondr');
  }

  public function solution()
  {
      $this->load->view('solution');
  }

  public function share()
  {
      $this->load->view('share');
  }

  public function review($page = 1)
  {
      $word = $this->input->get('word');
      $json = $this->submodel->reviewList($page);
      $this->load->view('real_review', array('list'=>$json['list'],'count'=>$json['count'],'word'=>$word));
  }

   public function reviewList($page = 1)
   {
       $json = $this->submodel->reviewList($page);
       $this->json($json);
   }

  public function notice($idx = 1)
  {
      $json = $this->submodel->noticeList($idx);
      $url = URL.'/sub/notice/';
      $this->paging($json, $url);
      $this->load->view('notice',array('list'=>$json['list']));
  }

  public function notice_view($idx)
  {
      $json = $this->submodel->noticeview($idx);
      $list = $this->submodel->noreply_list($idx);
      $this->load->view('notice_view',array('data'=>$json['data'],'next'=>$json['next'],'pre'=>$json['pre'],'list'=>$list));
  }

    public function noreply_list($idx)
    {
        $json = $this->submodel->noreply_list($idx);
        $this->json($json);
    }
   public function noticeList($idx = 1)
   {
       $json = $this->submodel->noticeList($idx);
       $this->json($json);
   }

  public function review_view($idx)
  {
      $json = $this->submodel->reviewData($idx);
      $list = $this->submodel->reply_list($idx);
      $this->load->view('review_view',array('data'=>$json['data'],'next'=>$json['next'],'pre'=>$json['pre'],'reply'=>$list));
  }
  public function insert_noreply()
  {
      $json = $this->submodel->insert_noreply();
      $this->json($json);
  }
  public function noreply_data($idx)
  {
      $json = $this->submodel->noreply_data($idx);
      $this->json($json);
  }
   public function reviewData($idx)
   {
       $json = $this->submodel->reviewData($idx);
       $this->json($json);
   }

  public function noreviewData($idx)
  {
      $json = $this->submodel->noreviewData($idx);
      $this->json($json);
  }
  public function review_like($idx)
  {
      $json = $this->submodel->review_like($idx);
      $this->json($json);
  }
  public function review_nolike($idx)
  {
      $json = $this->submodel->review_nolike($idx);
      $this->json($json);
  }
  public function news_like($idx)
  {
      $json = $this->submodel->news_like($idx);
      $this->json($json);
  }
  public function contactus()
  {
      $this->load->view('contactus');
  }

  public function solution_ms()
  {
      $this->load->view('solution_ms');
  }

  public function solution_rdr()
  {
      $this->load->view('solution_rdr');
  }

  public function solution_gw()
  {
      $this->load->view('solution_gw');
  }

  public function news($page = 1)
  {
      $word = $this->input->get('word');
      $json = $this->submodel->newsList($page);
      $this->load->view('news', array('list'=>$json['list'],'count'=>$json['count'],'word'=>$word));
  }

  public function news_view($idx)
  {
      $json = $this->submodel->newsData($idx);
      $list = $this->submodel->newsreply_list($idx);
      $this->load->view('news_view',array('data'=>$json['data'],'pre'=>$json['pre'],'next'=>$json['next'],'reply'=>$list));
  }

   public function insert_newsreply()
   {
       $json = $this->submodel->insert_newsreply();
       $this->json($json);
   }

  public function qna($idx = 1)
  {
      $json = $this->submodel->qna_list($idx);
      $url = URL.'/sub/qna/';
      $this->paging($json, $url);
      $array = array('list'=>$json['list']);
      $this->load->view('qna',$array);
  }

  public function newsreply_list($idx)
  {
      $json = $this->submodel->newsreply_list($idx);
      $this->json($json);
  }

  public function newsreply_data($idx)
  {
      $json = $this->submodel->newsreply_data($idx);
      $this->json($json);
  }

  public function newsreply_update($idx)
  {
      $json = $this->submodel->newsreply_update($idx);
      $this->json($json);
  }

  public function newsreply_del($idx)
  {
      $json = $this->submodel->newsreply_del($idx);
      $this->json($json);
  }

  // public function review()
  // {
  //     $json = $this->submodel->reply_list();
  //     $this->load->view('review',array('data'=>$json));
  // }

 public function insert_reply()
 {
     $json = $this->submodel->insert_reply();
     $this->json($json);
 }

 public function reply_like($idx)
 {
     $json = $this->submodel->reply_like($idx);
     $this->json($json);
 }

  public function reply_data($idx)
  {
      $json = $this->submodel->reply_data($idx);
      $this->json($json);
  }

  public function reply_update($idx)
  {
      $json = $this->submodel->reply_update($idx);
      $this->json($json);
  }

  public function noreply_update($idx)
  {
      $json = $this->submodel->noreply_update($idx);
      $this->json($json);
  }

  public function reply_del($idx)
  {
      $json = $this->submodel->reply_del($idx);
      $this->json($json);
  }

  public function noreply_del($idx)
  {
      $json = $this->submodel->noreply_del($idx);
      $this->json($json);
  }

   public function sendmail() {
      $json = $this->email();
      $this->json($json);
    }
//--news---------------------------------------------

 public function newsList($page = 1)
 {
     $json = $this->submodel->newsList($page);
     $this->json($json);
 }

// qna ----------------------------------------------------------

  public function insert_qna()
  {
      $json = $this->submodel->insert_qna();
      $this->json($json);
  }

  public function qna_list($idx)
  {
      $json = $this->submodel->qna_list($idx);
      $this->json($json);
  }

  public function kakao()
  {
      $this->load->view('kakao');
  }


}
