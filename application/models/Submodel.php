<?php
class Submodel extends CI_Model {
    function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->database();
        $this->table = "sample_admin";
    }

//faq ---------------------------------------------------------------

    public function faqList() {
      $sql="SELECT faq_idx, faq_title, faq_text FROM radondr_faq
              ORDER BY faq_idx DESC";
      return $this->db->query($sql, $array)->result();
    }

    public function reviewList($page) {
      $word = $this->input->get('word');
      if($word)
        $where = ' WHERE review_title like "%'.$word.'%"';
      $limit=6;
      $offset=$limit*($page-1);
      $sql="SELECT review_idx, review_title, review_text, admin_name,
              review_thumb, review_like, IFNULL(count,0) replycount
               FROM radondr_review JOIN radondr_admin USING(admin_idx)
               LEFT JOIN (
                 SELECT COUNT(*) count, rereply_idx, rereply_board FROM radondr_rereply
                   GROUP BY rereply_board
               ) as a ON rereply_board = review_idx
               ".$where."
               ORDER BY review_idx DESC
               limit {$limit} offset {$offset}";
      $result = $this->db->query($sql, $array)->result();

      $sql = "SELECT COUNT(*) count
            FROM radondr_review JOIN radondr_admin USING(admin_idx)
            LEFT JOIN (
              SELECT COUNT(*) count, rereply_idx, rereply_board FROM radondr_rereply
                GROUP BY rereply_board
            ) as a ON rereply_board = review_idx
            ".$where;
      $count = $this->db->query($sql, $array)->row()->count;
      $con = ceil($count/6);
      return array('return'=>true,'list'=>$result, 'count'=>$con);
    }

    public function reviewData($idx) {
      $sql="SELECT review_idx idx, review_title, review_text, admin_name,
              review_date, review_like, IFNULL(count,0) count
               FROM radondr_review JOIN radondr_admin
              USING(admin_idx)
              LEFT JOIN (
                SELECT COUNT(*) count, rereply_idx, rereply_board FROM radondr_rereply
                  GROUP BY rereply_board
              ) as a ON rereply_board = review_idx
               WHERE review_idx = ?";
      $array = array($idx);
      $result = $this->db->query($sql, $array)->row();
      $sql = "SELECT * FROM radondr_review WHERE review_idx = (SELECT MIN(review_idx) min FROM radondr_review WHERE review_idx > ?)";
      $array = array($idx);
      $min = $this->db->query($sql, $array)->row()->review_idx;

      $sql = "SELECT * FROM radondr_review WHERE review_idx = (SELECT MAX(review_idx) FROM radondr_review WHERE review_idx < ?)";
      $array = array($idx);
      $max = $this->db->query($sql, $array)->row()->review_idx;

      return array('data'=>$result,'next'=>$max,'pre'=>$min);
    }

    public function review_like($idx) {
      $array = array($idx);
      $sql="SELECT review_like FROM radondr_review WHERE review_idx = ?";
      $like = $this->db->query($sql, $array)->row()->review_like;
      $like += 1;
      $arrays['review_like'] = $like;
      $this->db->where('review_idx', $idx);
      $result = $this->db->update('radondr_review', $arrays);

      return array('return'=>$result,'like'=>$like);
    }
    public function review_nolike($idx) {
      $array = array($idx);
      $sql="SELECT review_like FROM radondr_review WHERE review_idx = ?";
      $like = $this->db->query($sql, $array)->row()->review_like;
      $like -= 1;
      $arrays['review_like'] = $like;
      $this->db->where('review_idx', $idx);
      $result = $this->db->update('radondr_review', $arrays);

      return array('return'=>$result,'like'=>$like);
    }

    public function news_like($idx) {
      $array = array($idx);
      $sql="SELECT news_like FROM radondr_news WHERE news_idx = ?";
      $like = $this->db->query($sql, $array)->row()->news_like;
      $like += 1;
      $arrays['news_like'] = $like;
      $this->db->where('news_idx', $idx);
      $result = $this->db->update('radondr_news', $arrays);

      return array('return'=>$result,'like'=>$like);
    }
//라돈 댓글-----------------------------------------------------------
    public function insert_reply() {
        foreach($_POST as $key => $value ){
          if($key != "rereply_idx") {
            $array[$key] = $value;
          }
        }
        $array['rereply_date'] = date('Y-m-d H:i:s');
        $result = $this->db->insert('radondr_rereply', $array);
        $insert_id = $this->db->insert_id();
        if($_POST['rereply_replyidx'] == "") {
          $array['rereply_replyidx'] = $insert_id;
        }
        else {
          return array('return'=>$result);
        }
        $this->db->where('rereply_idx', $insert_id);
        $result = $this->db->update('radondr_rereply', $array);
        $_SESSION['id'] = $_POST['rereply_id'];
        $_SESSION['pass'] = $_POST['rereply_password'];
        return array('return'=>$result);
    }

    public function reply_list($idx) {
      $sql="SELECT rereply_idx reply_idx, rereply_text reply_text
              , rereply_date reply_date, rereply_replyidx reply_replyidx
              , rereply_id reply_id, rereply_password reply_pass
              FROM radondr_rereply
              WHERE rereply_board = ? ORDER BY rereply_replyidx ASC";
      $array=array($idx);
      return $this->db->query($sql, $array)->result();
    }

    public function reply_like($idx) {
      $array = array($idx);
      $sql="SELECT rereply_like FROM radondr_rereply WHERE rereply_idx = ?";
      $like = $this->db->query($sql, $array)->row()->reply_like;
      $like += 1;
      $arrays['reply_like'] = $like;
      $this->db->where('reply_idx', $idx);
      $result = $this->db->update('radondr_reply', $arrays);

      return array('return'=>$result);
    }

    public function reply_data($idx) {
      $sql="SELECT rereply_idx, rereply_text, rereply_date, rereply_replyidx, rereply_password pass
              FROM radondr_rereply
              WHERE rereply_idx = ?";
      $array = array($idx);
      return $this->db->query($sql, $array)->row();
    }

    public function noreply_data($idx) {
      $sql="SELECT noreply_idx, noreply_text, noreply_date, noreply_replyidx, noreply_password pass
              FROM radondr_noreply
              WHERE noreply_idx = ?";
      $array = array($idx);
      return $this->db->query($sql, $array)->row();
    }

    public function newsreply_data($idx) {
      $sql="SELECT newsreply_idx, newsreply_text, newsreply_date, newsreply_replyidx, newsreply_password pass
              FROM radondr_newsreply
              WHERE newsreply_idx = ?";
      $array = array($idx);
      return $this->db->query($sql, $array)->row();
    }

    public function reply_update($idx) {
      $sql="UPDATE radondr_rereply SET rereply_text = ? WHERE rereply_idx =?";
      $array = array($_POST['rereply_text'],$idx);
      $result = $this->db->query($sql, $array);
      return array('return'=>$result);
    }
    public function noreply_update($idx) {
      $sql="UPDATE radondr_noreply SET noreply_text = ? WHERE noreply_idx =?";
      $array = array($_POST['noreply_text'],$idx);
      $result = $this->db->query($sql, $array);
      return array('return'=>$result);
    }
    public function newsreply_update($idx) {
      $sql="UPDATE radondr_newsreply SET newsreply_text = ? WHERE newsreply_idx =?";
      $array = array($_POST['newsreply_text'],$idx);
      $result = $this->db->query($sql, $array);
      return array('return'=>$result);
    }
    public function reply_del($idx) {
      $sql="DELETE FROM radondr_rereply WHERE rereply_idx = ?";
        $array = array($idx);
        $result = $this->db->query($sql, $array);

        $sql="DELETE FROM radondr_rereply WHERE rereply_replyidx = ?";
        $array = array($idx);
        $this->db->query($sql, $array);
        return array('return'=>$result);
    }
    public function noreply_del($idx) {
      $sql="DELETE FROM radondr_noreply WHERE noreply_idx = ?";
      $array = array($idx);
      $result = $this->db->query($sql, $array);

      $sql="DELETE FROM radondr_noreply WHERE noreply_replyidx = ?";
      $array = array($idx);
      $result = $this->db->query($sql, $array);
      return array('return'=>$result);

    }
    public function newsreply_del($idx) {
      $sql="DELETE FROM radondr_newsreply WHERE newsreply_idx = ?";
      $array = array($idx);
      $result = $this->db->query($sql, $array);

      $sql="DELETE FROM radondr_newsreply WHERE newsreply_replyidx = ?";
      $array = array($idx);
      $result = $this->db->query($sql, $array);
      return array('return'=>$result);

    }
///------------------------------------------------

  public function noticeList($page) {
    $word = $this->input->get('word');
    $type = $this->input->get('type');
    $where = '';
    if($word && $type) {
      switch($type) {
        case 1:
          $where = ' WHERE notice_text like "%'.$word.'%"';
          break;
        case 2:
          $where = ' WHERE notice_title like "%'.$word.'%"';
          break;
        case 3:
          $where = ' WHERE notice_title like "%'.$word.'%" || notice_text like "%'.$word.'%"';
          break;
      }
    }
    $limit=10;
    $offset=$limit*($page-1);
    $sql="SELECT notice_idx, notice_title, @rownum:=@rownum+1 num,
                     admin_name,notice_date
                     FROM radondr_notice
                     JOIN radondr_admin USING(admin_idx), (SELECT @rownum:=0) TMP
                     ".$where."
                     ORDER BY notice_idx DESC
                     limit {$limit} offset {$offset}";
    $result = $this->db->query($sql, $array)->result();

    $sql="SELECT COUNT(*) count FROM radondr_notice".$where;
    $count = $this->db->query($sql)->row()->count;
    return array('return'=>true,'list'=>$result, 'count'=>$count,'word'=>$word, 'type'=>$type);
  }

  public function noticeview($idx) {
    $sql="SELECT notice_idx, notice_title, notice_text
            , admin_name, notice_date FROM radondr_notice
            JOIN radondr_admin USING(admin_idx)
            WHERE notice_idx = ?";
    $array = array($idx);
    $result = $this->db->query($sql, $array)->row();

    $sql = "SELECT * FROM radondr_notice WHERE notice_idx = (SELECT MIN(notice_idx) min FROM radondr_notice WHERE notice_idx > ?)";
    $array = array($idx);
    $min = $this->db->query($sql, $array)->row()->notice_idx;

    $sql = "SELECT * FROM radondr_notice WHERE notice_idx = (SELECT MAX(notice_idx) FROM radondr_notice WHERE notice_idx < ?)";
    $array = array($idx);
    $max = $this->db->query($sql, $array)->row()->notice_idx;

    return array('data'=>$result,'next'=>$max,'pre'=>$min);
  }
//------------------------------------------------------------------

  public function insert_noreply() {
      foreach($_POST as $key => $value ){
        if($key != "noreply_idx") {
          $array[$key] = $value;
        }
      }
      $array['noreply_date'] = date('Y-m-d H:i:s');
      $result = $this->db->insert('radondr_noreply', $array);
      $insert_id = $this->db->insert_id();
      if($_POST['noreply_replyidx'] == "") {
        $array['noreply_replyidx'] = $insert_id;
      }
      else {
        return array('return'=>$result);
      }
      $this->db->where('noreply_idx', $insert_id);
      $result = $this->db->update('radondr_noreply', $array);
      $_SESSION['id'] = $_POST['noreply_id'];
      $_SESSION['pass'] = $_POST['noreply_password'];
      return array('return'=>$result);
  }

  public function noreply_list($idx) {
    $sql="SELECT noreply_idx, noreply_date,
            noreply_replyidx, noreply_id, noreply_password, noreply_del FROM radondr_noreply
            WHERE noreply_board = ? ORDER BY noreply_replyidx ASC";
    $array=array($idx);
    return $this->db->query($sql, $array)->result();
  }

// news --------------------------------------------------------------------
    public function newsList($page) {
      $word = $this->input->get('word');
      if($word)
        $where = ' WHERE news_title like "%'.$word.'%"';
      $limit=6;
      $offset=$limit*($page-1);
      $sql="SELECT news_idx, news_title, news_text, admin_name,
              news_thumb, news_like, IFNULL(count,0) replycount
               FROM radondr_news JOIN radondr_admin USING(admin_idx)
               LEFT JOIN (
                 SELECT COUNT(*) count, newsreply_idx, news_board FROM radondr_newsreply
                   GROUP BY news_board
               ) as a ON news_board = news_idx
               ".$where."
               ORDER BY news_idx DESC
               limit {$limit} offset {$offset}";
      $result = $this->db->query($sql, $array)->result();

      $sql = "SELECT COUNT(*) count
            FROM radondr_news JOIN radondr_admin USING(admin_idx)
            LEFT JOIN (
              SELECT COUNT(*) count, newsreply_idx, news_board FROM radondr_newsreply
                GROUP BY news_board
            ) as a ON news_board = news_idx
            ".$where;
      $count = $this->db->query($sql, $array)->row()->count;
      $con = ceil($count/6);
      return array('return'=>true,'list'=>$result, 'count'=>$con);
    }

    public function newsData($idx) {
      $sql="SELECT news_idx idx, news_title, news_text, admin_name,
              news_date, news_like, IFNULL(count,0) count
               FROM radondr_news JOIN radondr_admin
              USING(admin_idx)
              LEFT JOIN (
                SELECT COUNT(*) count, newsreply_idx, news_board FROM radondr_newsreply
                  GROUP BY news_board
              ) as a ON news_board = news_idx
               WHERE news_idx = ?";
      $array = array($idx);
      $result = $this->db->query($sql, $array)->row();
      $sql = "SELECT * FROM radondr_news WHERE news_idx = (SELECT MIN(news_idx) min FROM radondr_news WHERE news_idx > ?)";
      $array = array($idx);
      $min = $this->db->query($sql, $array)->row()->news_idx;

      $sql = "SELECT * FROM radondr_news WHERE news_idx = (SELECT MAX(news_idx) FROM radondr_news WHERE news_idx < ?)";
      $array = array($idx);
      $max = $this->db->query($sql, $array)->row()->news_idx;

      return array('data'=>$result,'next'=>$max,'pre'=>$min);
    }

    public function insert_newsreply() {
        $sql = "INSERT INTO radondr_newsreply (newsreply_id, newsreply_password, newsreply_text, news_board, newsreply_replyidx, newsreply_date) VALUES (?, ?, ?, ?, ?, now())";
        $array = array($_POST['newsreply_id'],$_POST['newsreply_password'],$_POST['newsreply_text'],$_POST['newsreply_board'],$_POST['newsreply_replyidx']);
        $result = $this->db->query($sql,$array);
        $insert_id = $this->db->insert_id();
        if($_POST['newsreply_replyidx'] != "") {
            return array('return'=>$result);
        }

        $sql = "UPDATE radondr_newsreply SET newsreply_replyidx = ? WHERE newsreply_idx = ?";
        $array = array($insert_id,$insert_id);
        $result = $this->db->query($sql,$array);

        $_SESSION['id'] = $_POST['newsreply_id'];
        $_SESSION['pass'] = $_POST['newsreply_password'];
        return array('return'=>$result);
      }

      public function newsreply_list($idx) {
        $sql="SELECT newsreply_idx, newsreply_text, newsreply_date,
                newsreply_replyidx, newsreply_id, newsreply_password, newsreply_del FROM radondr_newsreply
                WHERE news_board = ? ORDER BY newsreply_replyidx ASC";
        $array=array($idx);
        return $this->db->query($sql, $array)->result();
      }
// qna -----------------------------------------------------------------------
    public function insert_qna() {
      $sql = "INSERT INTO radondr_qna (qna_id, qna_password, qna_text, qna_date) VALUES (?, ?, ?, now())";
      $array = array($_POST['qna_id'],$_POST['qna_password'],$_POST['qna_text']);
      $result = $this->db->query($sql,$array);
      $insert_id = $this->db->insert_id();

      $sql = "UPDATE radondr_qna SET qna_qnaidx = ? WHERE qna_idx = ?";
      $array = array($insert_id,$insert_id);
      $result = $this->db->query($sql,$array);

      $_SESSION['id'] = $_POST['qna_id'];
      $_SESSION['pass'] = $_POST['qna_password'];
      return array('return'=>$result);
    }

    public function qna_list($page) {
      $limit=10;
      $offset=$limit*($page-1);
      $sql="SELECT @rownum := @rownum+1 AS num, A.*
        FROM (
            SELECT qna_idx, qna_text, qna_id, qna_date, qna_reply, qna_qnaidx
              FROM radondr_qna
              JOIN ( SELECT @rownum := 0) R
            ) A
            ORDER BY qna_qnaidx DESC
            limit {$limit} offset {$offset}";
      $list = $this->db->query($sql)->result();

      $sql="SELECT COUNT(*) count FROM radondr_qna";
      $count = $this->db->query($sql)->row()->count;
      return array('list'=>$list,'count'=>$count);
    }
    public function kakaoinfo(){
      $ch=curl_init();
      $header = "Bearer ".$_POST['idtoken'];
      $url="https://kapi.kakao.com/v2/user/me";
      $ch=curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $headers[] = 'Authorization: '.$header;
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      $response=curl_exec($ch);
      $status_code=curl_getinfo($ch, CURLINFO_HTTP_CODE);
      if($status_code == "200"){
          return array('data'=>$response);
      }
      else{
          return array('data'=>false);
      }
  }
  public function naverinfo(){
    $client_id = '클라이언트ID';
    $client_secret = '시크릿KEY';
    $ch=curl_init();
    $url="https://nid.naver.com/oauth2.0/token?client_id=".$client_id."&client_secret=".
    $client_secret."&grant_type=authorization_code&state=".$_GET['state']."&code=".$_GET['code'];
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response=curl_exec($ch);
    $array = json_decode($response);
    $token = $array->access_token;
    if($token){
        $header = "Bearer ".$token;
        $url="https://openapi.naver.com/v1/nid/me";
        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $headers[] = 'Authorization: '.$header;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response=curl_exec($ch);
        $status_code=curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if($status_code == "200"){
            return array('data'=>$response);
        }
        else{
            return array('data'=>false);
        }
    }
}

}
?>
