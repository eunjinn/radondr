<?php
class adminmodel extends CI_Model {
    function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->database();
        $this->table = "radondr_admin";
    }
// 로그인 ---------------------------------------
    public function insert_login() {
        // $_POST['id'] = 'sample';
        // $_POST['pass'] = 'admin';
        // $_POST['name'] = '샘플';
        foreach($_POST as $key => $value ){
            $key = 'admin_'.$key;
            if($key == "admin_pass") {
                $value = password_hash($value , PASSWORD_DEFAULT, ['cost'=>12]);
            }
            $array[$key] = $value;
        }
        $result = $this->db->insert($this->table, $array);
        return array('return'=>$result);
    }

    public function loginData() {
      // $_POST['id'] = 'callget';
      // $_POST['pass'] = 'admin';
      $sql="SELECT admin_idx, admin_id, admin_pass, admin_name
            FROM radondr_admin WHERE admin_id = ?";
      $array = array($_POST['id']);
      $data = $this->db->query($sql, $array)->row();

      if (password_verify($_POST['pass'] ,$data->admin_pass) ) {
          $_SESSION['id'] = $_POST['id'];
          $_SESSION['idx'] = $data->admin_idx;
          $_SESSION['name'] = $data->admin_name;
          return array('return'=>true);
      }
      else {
          return array('return'=>false);
      }
    }
//------------------------------------------------

//리스트------------------------------------------
    public function reviewListData($page = 1) {
        $table = "radondr_review";
        $limit=10;
        $sql="SELECT @rownum := @rownum+1 AS num, A.*
        FROM (
              SELECT review_idx as idx, review_title, admin_name,
                review_like, review_date
              FROM radondr_review
              LEFT JOIN radondr_admin USING(admin_idx)
              JOIN ( SELECT @rownum := 0) R
            ) A ORDER BY idx DESC";
        $result = $this->db->query($sql)->result();

        return array('return'=>true,'list'=>$result);
    }

    public function reviewInsert() {
        $table = "radondr_review";
        foreach($_POST as $key => $value ){
            $array[$key] = $value;
        }
        $array['review_date'] = date('Y-m-d H:i:s');
        $result = $this->db->insert($table, $array);
        return array('return'=>$result);
    }

    public function reviewData($idx) {
      $array = array($idx);
      $sql="SELECT review_idx, review_title, review_text,
            review_thumb  FROM radondr_review
              WHERE review_idx = ?";
      return $this->db->query($sql, $array)->row();
    }

    public function reviewUpdate($idx) {
      foreach($_POST as $key => $value ){
          $array[$key] = $value;
      }
      $this->db->where('review_idx', $idx);
      $result = $this->db->update('radondr_review', $array);
      return array('return'=>$result);
    }

    public function reviewDel($idx) {
      $array = array($idx);
      $sql= "DELETE FROM radondr_review WHERE review_idx = ?";
      $result = $this->db->query($sql, $array);
      return array('return'=>$result);
    }
//------------------------------------------------

// 공지사항 ------------------------------------------------

    public function noticeInsert() {
        $table = "radondr_notice";
        foreach($_POST as $key => $value ){
            $array[$key] = $value;
        }
        $array['notice_date'] = date('Y-m-d H:i:s');
        $result = $this->db->insert($table, $array);
        return array('return'=>$result);
    }

    public function noticeListData($page = 1) {
        $table = "radondr_notice";
        $limit=10;
        $sql="SELECT @rownum := @rownum+1 AS num, A.*
        FROM (
              SELECT notice_idx as idx, notice_title, admin_name,
                notice_like, notice_date
              FROM radondr_notice
              LEFT JOIN radondr_admin USING(admin_idx)
              JOIN ( SELECT @rownum := 0) R
            ) A ORDER BY idx DESC";
        $result = $this->db->query($sql)->result();

        return array('return'=>true,'list'=>$result);
    }

    public function noticeData($idx) {
      $array = array($idx);
      $sql="SELECT notice_idx, notice_title, notice_text FROM radondr_notice
              WHERE notice_idx = ?";
      return $this->db->query($sql, $array)->row();
    }

    public function noticeUpdate($idx) {
      foreach($_POST as $key => $value ){
          $array[$key] = $value;
      }
      $this->db->where('notice_idx', $idx);
      $result = $this->db->update('radondr_notice', $array);
      return array('return'=>$result);
    }

    public function noticeDel($idx) {
      $array = array($idx);
      $sql= "DELETE FROM radondr_notice WHERE notice_idx = ?";
      $result = $this->db->query($sql, $array);
      return array('return'=>$result);
    }
// 전문가 소식 --------------------------------------

  public function expertListData($page = 1) {
      $table = "radondr_expert";
      $limit=10;
      $sql="SELECT @rownum := @rownum+1 AS num, A.*
      FROM (
            SELECT expert_idx as idx, expert_title, admin_name,
              expert_like, expert_date
            FROM radondr_expert
            LEFT JOIN radondr_admin USING(admin_idx)
            JOIN ( SELECT @rownum := 0) R
          ) A ORDER BY idx DESC";
      $result = $this->db->query($sql)->result();

      return array('return'=>true,'list'=>$result);
  }

  public function expertInsert() {
      $table = "radondr_expert";
      foreach($_POST as $key => $value ){
          $array[$key] = $value;
      }
      $array['expert_date'] = date('Y-m-d H:i:s');
      $result = $this->db->insert($table, $array);
      return array('return'=>$result);
  }

  public function expertData($idx) {
    $array = array($idx);
    $sql="SELECT expert_idx, expert_title, expert_text FROM radondr_expert
            WHERE expert_idx = ?";
    return $this->db->query($sql, $array)->row();
  }

  public function expertUpdate($idx) {
    foreach($_POST as $key => $value ){
        $array[$key] = $value;
    }
    $this->db->where('expert_idx', $idx);
    $result = $this->db->update('radondr_expert', $array);
    return array('return'=>$result);
  }

  public function expertDel($idx) {
    $array = array($idx);
    $sql= "DELETE FROM radondr_expert WHERE expert_idx = ?";
    $result = $this->db->query($sql, $array);
    return array('return'=>$result);
  }
//faq-----------------------------------------------------------

  public function faqInsert() {
      $table = "radondr_faq";
      foreach($_POST as $key => $value ){
          $array[$key] = $value;
      }
      $array['faq_date'] = date('Y-m-d H:i:s');
      $result = $this->db->insert($table, $array);
      return array('return'=>$result);
  }

  public function faqListData($page = 1) {
      $table = "radondr_faq";
      $limit=10;
      $sql="SELECT @rownum := @rownum+1 AS num, A.*
      FROM (
            SELECT faq_idx as idx, faq_title, admin_name,
              faq_like, faq_date
            FROM radondr_faq
            LEFT JOIN radondr_admin USING(admin_idx)
            JOIN ( SELECT @rownum := 0) R
          ) A ORDER BY idx DESC";
      $result = $this->db->query($sql)->result();

      return array('return'=>true,'list'=>$result);
  }

  public function faqData($idx) {
    $array = array($idx);
    $sql="SELECT faq_idx, faq_title, faq_text FROM radondr_faq
            WHERE faq_idx = ?";
    return $this->db->query($sql, $array)->row();
  }

  public function faqUpdate($idx) {
    foreach($_POST as $key => $value ){
        $array[$key] = $value;
    }
    $this->db->where('faq_idx', $idx);
    $result = $this->db->update('radondr_faq', $array);
    return array('return'=>$result);
  }

  public function faqDel($idx) {
    $array = array($idx);
    $sql= "DELETE FROM radondr_faq WHERE faq_idx = ?";
    $result = $this->db->query($sql, $array);
    return array('return'=>$result);
  }
//--------------------------------------------------------------

// news ---------------------------------------------------------
  public function newsListData($page = 1) {
      $table = "radondr_news";
      $limit=10;
      $sql="SELECT @rownum := @rownum+1 AS num, A.*
      FROM (
            SELECT news_idx as idx, news_title, admin_name,
              news_like, news_date
            FROM radondr_news
            LEFT JOIN radondr_admin USING(admin_idx)
            JOIN ( SELECT @rownum := 0) R
          ) A ORDER BY idx DESC";
      $result = $this->db->query($sql)->result();

      return array('return'=>true,'list'=>$result);
  }

  public function newsInsert() {
      $sql = "INSERT INTO radondr_news (news_title, news_text, news_thumb, admin_idx, news_date) VALUES (?, ?, ?, ?, now())";
      $array = array($_POST['news_title'],$_POST['news_text'],$_POST['news_thumb'],$_POST['admin_idx']);
      $result = $this->db->query($sql,$array);
      return array('return'=>$result);
  }

  public function newsData($idx) {
    $array = array($idx);
    $sql="SELECT news_idx, news_title, news_text,
          news_thumb  FROM radondr_news
            WHERE news_idx = ?";
    return $this->db->query($sql, $array)->row();
  }

  public function newsUpdate($idx) {
    $sql = "UPDATE radondr_news SET news_title = ?, news_text = ?, news_thumb = ?, admin_idx = ? WHERE news_idx = ?";
    $array = array($_POST['news_title'],$_POST['news_text'],$_POST['news_thumb'],$_POST['admin_idx'],$_POST['news_idx']);
    $result = $this->db->query($sql,$array);
    return array('return'=>$result);
  }

  public function newsDel($idx) {
    $array = array($idx);
    $sql= "DELETE FROM radondr_news WHERE news_idx = ?";
    $result = $this->db->query($sql, $array);
    return array('return'=>$result);
  }
  //qna --------------------------------------------------------------------
  public function qnaListData($page = 1) {
      $table = "radondr_qna";
      $limit=10;
      $sql="SELECT @rownum := @rownum+1 AS num, A.*
      FROM (
            SELECT qna_idx as idx, qna_text, qna_id, qna_date, qna_reply
            FROM radondr_qna
            JOIN ( SELECT @rownum := 0) R WHERE qna_reply = 0
          ) A ORDER BY idx DESC";
      $result = $this->db->query($sql)->result();

      return array('return'=>true,'list'=>$result);
  }

  public function qnaData($idx) {
    $array = array($idx);
    $sql="SELECT qna_idx, qna_id, qna_text FROM radondr_qna
            WHERE qna_idx = ?";
    $data = $this->db->query($sql, $array)->row();

    $sql="SELECT qna_idx, qna_id, qna_text FROM radondr_qna
            WHERE qna_qnaidx = ? AND qna_reply = 1";
    $reply = $this->db->query($sql, $array)->row();
    return array('data'=>$data,'reply'=>$reply);
  }

  public function qnaInsert() {
      $sql = "INSERT INTO radondr_qna (qna_text, qna_qnaidx, qna_reply, qna_date
      , qna_id) VALUES (?,?,1,now(),'라돈닥터')";
      $array = array($_POST['qnaAnswer_text'],$_POST['idx']);
      $result = $this->db->query($sql,$array);
      return array('return'=>$result);
  }

  public function qnaUpdate($idx) {
    $sql = "UPDATE radondr_qna SET qna_text = ? WHERE qna_qnaidx = ?
            AND qna_reply = 1";
    $array = array($_POST['qnaAnswer_text'],$idx);
    $result = $this->db->query($sql,$array);
    return array('return'=>$result);
  }

  public function qnaDel($idx) {
    $array = array($idx);
    $sql= "DELETE FROM radondr_qna WHERE qna_qnaidx = ?";
    $result = $this->db->query($sql, $array);
    return array('return'=>$result);
  }

}
?>
