<html>
  <head>
  </head>
  <body>
    <form>
      아이디 : <input type="text" name="reply_id" value="<?=$_SESSION['id']?>" <?if($_SESSION['id']) { echo 'readonly';}?>>
      비밀번호 : <input type="password" name="reply_password" value="<?=$_SESSION['pass']?>" <?if($_SESSION['pass']) { echo "readonly";}?>>
      이름 : <input type="text" name="reply_name" id="names">
      내용 : <textarea name="reply_text" id="texts"></textarea>
      <input type="hidden" name="reply_board" value="1">
      <input type="hidden" name="reply_replyidx" value = "">
      <input type="hidden" name="reply_idx" id="idxs" value = "">
      <button type="button" id="button">전송</button>
    </form>


    <table>
      <tr>
        <td>번호</td><td>내용</td><td>글쓴이</td><td>좋아요갯수</td><td>날짜</td><td>버튼</td>
      </tr>

      <?foreach($data as $row) {?>
        <?$re =""; if($row->reply_idx != $row->reply_replyidx){ $re = "ㄴ";}?>
        <tr data-idx = "<?=$row->reply_idx?>">
          <td><?=$re?><?=$row->reply_idx?></td>
          <td><?=$row->reply_text?></td>
          <td><?=$row->reply_name?></td>
          <td><?=$row->reply_like?></td>
          <td><?=$row->reply_date?></td>
          <td><button class="like">좋아요</button> | <?if($row->reply_id == $_SESSION['id'] && $row->reply_password == $_SESSION['pass']) {?> <button type="button" class="edit">수정</button> <button type="button" class="del">삭제</button>  <?}?> </td>
        </tr>
      <?}?>
    </table>
    <form>
        <table>
          <tr>
            <td>성함</td><td><input type="text" id="name" class="req" required="required"></td>
          </tr>
          <tr>
            <td>회사/조직</td><td><input type="text" id="company" class="req" required="required"></td>
          </tr>
          <tr>
            <td>전화번호</td><td><input type="text" id="phone" class="req" required="required"></td>
          </tr>
          <tr>
            <td>이메일 주소</td><td><input type="email" id="email" class="req" required="required"></td>
          </tr>
          <tr>
            <td>요청사항</td><td><input type="text" id="request" class="req" required="required"></td>
          </tr>
        </table>
        <button type="button" id="contact">보내기</button>
        <button typr="submit" style="display:none;">보내기</button>
    </form>

    <!-- jQuery -->
    <script src="/assets/template/jquery.min.js"></script>
    <script src="/assets/js/common.js"></script>
    <script>
      $('body').on('click', '#button', function() {
        var form = $('form')[0];
        var data = new FormData(form);
        var jsons = jsonreturn('/sample/insert_reply',data);
        if(jsons.return == true) {
          alert("등록되었습니다");
        }
      });

      $('body').on('click', '.like', function() {
        var idx = $(this).parent().parent().attr('data-idx');
        var jsons = jsonreturn('/sample/reply_like/'+idx);
        if(jsons.return == true) {
          location.reload();
        }
      });

      $('body').on('click', '.edit', function() {
        var idx = $(this).parent().parent().attr('data-idx');
        var jsons = jsonreturn('/sample/reply_data/'+idx);
        console.log(jsons);
        $('#names').val(jsons.reply_name);
        $('#texts').val(jsons.reply_text);
        $('#idxs').val(jsons.reply_idx);
        $('#button').attr('id','edits').text('수정');
      });

      $('body').on('click', '#edits', function() {
        var idx = $('#idxs').val();
        var name = $('#names').val();
        var text = $('#texts').val();
        var data = {'name':name, 'text':text};
        var jsons = json('/sample/reply_update/'+idx,data);
        location.reload();

      });

      $('body').on('click', '.del', function() {
        var idx = $(this).parent().parent().attr('data-idx');
        var jsons = jsonreturn('/sample/reply_del/'+idx);
        location.reload();
      });

    $('body').on('click', '#contact', function() {
      var check = true;
       $('.req').each(function() {
       if (!$(this).val()) {
             check = false;
             return false;
           }
        });
      if(check){
        var title = "요청사항입니다";
        var email = $('#email').val();
        var content = '성함 : '+$('#name').val()+'\n회사/조직 : '+$('#company').val()+'\n전화번호 :'+$('#phone').val()+'\n요청사항 : '+$('#request').val();
        var data = {'title':title,'text':content,'email':email};
        var result = json('/sample/sendmail',data);
      }
      else{
        $(this).next().trigger('click');
      }
    });
  </script>

  </body>
</html>
