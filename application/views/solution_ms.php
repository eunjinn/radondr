<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">

    <meta name="keywords" content="radon Dr.">
    <meta name="description" content="Global Total Radon Solution">

    <link href="/assets/images/favicon/favicon.png" rel="shortcut icon" type="image/x-icon">
    <link href="/assets/images/favicon/favicon.png" rel="icon" type="image/x-icon">

    <title>radon Dr.</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/lib/animate/animate.css" rel="stylesheet" type="text/css">

    <link href="/assets/css/global.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/sub.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/solutions.css" rel="stylesheet" type="text/css">
</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>
    <!-- Preloader_END -->

    <!-- Navigation -->
    <header>
        <nav class="navbar navbar-global navbar-fixed-top" role="navigation">
            <div class="container p0">
                <div class="logo">
                    <a href="/">
                        <img src="/assets/images/logo_w.png" title="로고" alt="로고"  />
                    </a>
                </div>

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="custom-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="/sub/radondr">라돈닥터</a>
                        </li>
                        <li>
                            <a href="/sub/solution">솔루션</a>
                        </li>
                        <li>
                            <a href="/sub/review">저감사례</a>
                        </li>
                        <li>
                            <a href="/sub/news">뉴스</a>
                        </li>
                        <li>
                            <a href="/sub/qna">Q&A</a>
                        </li>
                        <li>
                            <a href="/sub/contactus">문의하기</a>
                        </li>
                        <li>
                            <a href="http://www.geomall.kr/" target="_blank">쇼핑몰</a>
                        </li>
                    </ul>
                </div>
            </div><!-- Container_END -->
        </nav>
    </header>
    <!-- Navigation_END -->

    <!-- Solution -->
    <section id="solution" class="wow fadeInUp">
        <div class="container">
            <div class="section-header">
                <p>라돈 저감 솔루션(Radon Mitigation Solution)</p>
                <h3>radon Dr. MS</h3>
            </div>
            <div class="row solution">
                <div class="sol_1">
                    <div class="col-xs-12">
                        <p>
                            "라돈 저감은 토양과 건물내의 압력차이를 반대로 바꾸는 것에서 출발합니다. 또한 정기적인 검진을 통해 실내 라돈 농도를 관리하는 것이 중요하며 라돈닥터의 Radon Free Zone은 체계적이고 효율적인 관리를 위해 준비된 프로그램입니다."
                        </p>
                        <p>
                            실내 라돈 농도는 환기율과 압력의 변화에 직접적인 영향을 받습니다. 건물 내에 형성되는 음압(negative pressure)에 영향을 주는 인위적인 요소들은 연소기기(combustion appliances – 가스렌지, 오븐, 스토브, 히터, 온수기, 빨래 건조기, 벽난로 등)와 HVAC (Heating, Ventilation, Air Conditioning) 시스템입니다. 토양을 감압시키거나 건물을 가압하는 것은 방법론상의 차이일 뿐, 같은 맥락입니다. 건물을 가압시키기 위해서는 얼마만큼의 공기가 더 필요하고, 그리고 그 공기를 어디에서 가져오며 (위층 또는 외부), 만약 외부에서 가져온다면 어떻게 조건화 (냉.난방등) 시키는가 하는 점이 고려돼야 합니다.
                        </p>
                    </div>
                </div>
                <div class="sol_2 wow fadeInUp">
                    <div class="row m0 mb50">
                        <div class="step ms">
                            <img src="/assets/images/solution/stethoscope.png" alt="">
                            <p>Ⅰ. 검사</p>
                            <p>
                                1) 간시측정, 장기 측정 기획<br>
                                2) 측정 환경 분석<br>
                                3) 외적 변수 체크리스트<br>
                                4) Detector 및 측정방식 결정<br>
                                5) Derector Positioning
                            </p>
                        </div>
                        <div class="step ms">
                            <img src="/assets/images/solution/magnifying-glass.png" alt="">
                            <p>Ⅱ. 판독</p>
                            <p>
                                1) Data Report<br>
                                2) 측정 환경, 외적 변수 Report<br>
                                3) Data Analyssis<br>
                                4) Result Report<br>
                            </p>
                        </div>
                        <div class="step ms">
                            <img src="/assets/images/solution/report.png" alt="">
                            <p>Ⅲ. 진단</p>
                            <p>
                                1) Data Report<br>
                                2) Data Analysis<br>
                                3) Diagnosis<br>
                                4) Certification
                            </p>
                        </div>
                        <div class="step ms">
                            <img src="/assets/images/solution/tablets.png" alt="">
                            <p>Ⅳ. 처방</p>
                            <p>
                                1) 실내 구조 분석<br>
                                2) 외부 환경 및 토양분석<br>
                                3) Radon Mitigation Method 결정<br>
                                4) Mitigation System 설계<br>
                                5) Mitigation System 설계 시공<br>
                            </p>
                        </div>
                        <div class="step ms">
                            <img src="/assets/images/solution/medical-kit.png" alt="">
                            <p>Ⅴ. 정기검진</p>
                            <p>
                                1) Detector 시공<br>
                                2) zephyAURA(Premium Service)<br>
                                3) Data monitoring<br>
                                4) Diagnosis<br>
                                5) Emergency Treatment
                            </p>
                        </div>
                    </div>
                </div>
                <div class="sol_3">
                    <button class="btn" onclick="location.href='/sub/solution'"><i class="fas fa-arrow-left"></i> 솔루션으로 돌아가기</button>
                </div>
            </div>
        </div>
    </section>
    <!-- Solution_END -->

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 p0">
                    <div class="foot_logo">
                        <img src="/assets/images/logo_w.png" title="로고" alt="로고">
                    </div>
                    <div class="foot_cont">
                        상호명 : C&H, Inc | 주소 : 서울특별시 강남구 강남대로 320, 황화빌딩 1505호<br>
                        대표전화 : (+82) 02-554-3869 | 팩스 : (+82) 02-556-0480 | 이메일 : radon@candh.co.kr
                        <span class="m_link">
                            <a href="http://www.newturntree.com/" target="_blank" title="뉴턴트리 홈페이지로 이동" alt="뉴턴트리 홈페이지로 이동">Newturn Tree</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Video</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Studio</a>
                        </span>
                    </div>
                    <div class="foot_contact">
                        <button onclick="location.href='/sub/contactus'" title="Contact Us로 이동" alt="Contact Us로 이동">
                            <i class="fal fa-paper-plane"></i>
                        </button>
                    </div>
                </div>
                <div class="col-xs-12 copyright">
                    <p>Copyright &copy; radon Dr. All rights reserved.</p>
                </div>
            </div><!-- row end -->
        </div><!-- Container_END -->
    </footer>

    <!-- Float-btn -->
    <div class="float-btn">
        <a href="/sub/qna">
            <img src="/assets/images/main/qna-icon-g.png" alt="">
        </a>
    </div>
    <!-- Float-btn_END-->

    <!-- Scroll to top -->
    <div class="scroll-up" title="TOP" alt="TOP">
        <a href="#solution"><span class="glyphicon glyphicon-menu-up"></span></a>
    </div>
    <!-- Scroll to top end-->

    <!-- Javascript files -->
    <script src="/assets/lib/wow/wow.min.js"></script>
    <script src="/assets/lib/isotope/isotope.pkgd.min.js"></script>

    <script src="/assets/js/sub.js"></script>
</body>
</html>
