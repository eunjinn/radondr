<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">

    <meta name="keywords" content="radon Dr.">
    <meta name="description" content="Global Total Radon Solution">

    <link href="/assets/images/favicon/favicon.png" rel="shortcut icon" type="image/x-icon">
    <link href="/assets/images/favicon/favicon.png" rel="icon" type="image/x-icon">

    <title>radon Dr.</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/lib/animate/animate.css" rel="stylesheet" type="text/css">

    <link href="/assets/css/global.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/sub.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/solutions.css" rel="stylesheet" type="text/css">
</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>
    <!-- Preloader_END -->

    <!-- Navigation -->
    <header>
        <nav class="navbar navbar-global navbar-fixed-top" role="navigation">
            <div class="container p0">
                <div class="logo">
                    <a href="/">
                        <img src="/assets/images/logo_w.png" title="로고" alt="로고"  />
                    </a>
                </div>

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="custom-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="/sub/radondr">라돈닥터</a>
                        </li>
                        <li>
                            <a href="/sub/solution">솔루션</a>
                        </li>
                        <li>
                            <a href="/sub/review">저감사례</a>
                        </li>
                        <li>
                            <a href="/sub/news">뉴스</a>
                        </li>
                        <li>
                            <a href="/sub/qna">Q&A</a>
                        </li>
                        <li>
                            <a href="/sub/contactus">문의하기</a>
                        </li>
                        <li>
                            <a href="http://www.geomall.kr/" target="_blank">쇼핑몰</a>
                        </li>
                    </ul>
                </div>
            </div><!-- Container_END -->
        </nav>
    </header>
    <!-- Navigation_END -->

    <!-- Solution -->
    <section id="solution" class="wow fadeInUp">
        <div class="container">
            <div class="section-header">
                <p>라돈 저감 솔루션(Radon Mitigation Solution)</p>
                <h3>radon Dr. GW</h3>
            </div>
            <div class="row solution">
                <div class="sol_1">
                    <div class="col-xs-12">
                        <p>
                            "지하수 속 라돈, radon Dr. GW 솔루션이면 안심하실 수 있습니다."
                        </p>
                        <p>
                            라돈 닥터는 무동력 지하수 라돈저감장치 Radon Free를 독점 보유하고 있습니다(특허등록 제 10-0911416). 또한 지하수 소독장치를 함께 시공하면 미생물 살균까지 가능하며 소규모 급수시설로도 가능합니다.
                        </p>
                    </div>
                </div>
                <div class="sol_2 wow fadeInUp">
                    <div class="row m0 mb50">
                        <div class="step gw">
                            <img src="/assets/images/solution/stethoscope.png" alt="">
                            <p>Ⅰ. 검사</p>
                            <p>
                                1) 측정 환경 분석<br>
                                2) 외적 변수 체크리스트<br>
                                3) 사료 채취 및 Report<br>
                                4) Radon 농도 측정
                            </p>
                        </div>
                        <div class="step gw">
                            <img src="/assets/images/solution/report.png" alt="">
                            <p>Ⅱ. 진단</p>
                            <p>
                                1) Data Report<br>
                                2) 환경 및 외적 변수 Report<br>
                                3) Radon Mitigation Method 결정
                            </p>
                        </div>
                        <div class="step gw">
                            <img src="/assets/images/solution/tablets.png" alt="">
                            <p>Ⅲ. 처방</p>
                            <p>
                                1) Radon Free 설계<br>
                                2) Radon Free 설계<br>
                                3) Radon Free Customizing
                            </p>
                        </div>
                        <div class="step gw">
                            <img src="/assets/images/solution/medical-kit.png" alt="">
                            <p>Ⅳ. 정기검진</p>
                            <p>
                                1) Data Monitoring<br>
                                2) Diagnosis<br>
                                3) Sampling & Measuring
                            </p>
                        </div>
                    </div>
                </div>
                <div class="sol_3">
                    <button class="btn" onclick="location.href='/sub/solution'"><i class="fas fa-arrow-left"></i> 솔루션으로 돌아가기</button>
                </div>
            </div>
        </div>
    </section>
    <!-- Solution_END -->

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 p0">
                    <div class="foot_logo">
                        <img src="/assets/images/logo_w.png" title="로고" alt="로고">
                    </div>
                    <div class="foot_cont">
                        상호명 : C&H, Inc | 주소 : 서울특별시 강남구 강남대로 320, 황화빌딩 1505호<br>
                        대표전화 : (+82) 02-554-3869 | 팩스 : (+82) 02-556-0480 | 이메일 : radon@candh.co.kr
                        <span class="m_link">
                            <a href="http://www.newturntree.com/" target="_blank" title="뉴턴트리 홈페이지로 이동" alt="뉴턴트리 홈페이지로 이동">Newturn Tree</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Video</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Studio</a>
                        </span>
                    </div>
                    <div class="foot_contact">
                        <button onclick="location.href='/sub/contactus'" title="Contact Us로 이동" alt="Contact Us로 이동">
                            <i class="fal fa-paper-plane"></i>
                        </button>
                    </div>
                </div>
                <div class="col-xs-12 copyright">
                    <p>Copyright &copy; radon Dr. All rights reserved.</p>
                </div>
            </div><!-- row end -->
        </div><!-- Container_END -->
    </footer>

    <!-- Float-btn -->
    <div class="float-btn">
        <a href="/sub/qna">
            <img src="/assets/images/main/qna-icon-g.png" alt="">
        </a>
    </div>
    <!-- Float-btn_END-->

    <!-- Scroll to top -->
    <div class="scroll-up" title="TOP" alt="TOP">
        <a href="#solution"><span class="glyphicon glyphicon-menu-up"></span></a>
    </div>
    <!-- Scroll to top end-->

    <!-- Javascript files -->
    <script src="/assets/lib/wow/wow.min.js"></script>
    <script src="/assets/lib/isotope/isotope.pkgd.min.js"></script>

    <script src="/assets/js/sub.js"></script>
</body>
</html>
