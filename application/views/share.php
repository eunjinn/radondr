<html>
<head>
  <title>Your Website Title</title>
    <!-- You can use Open Graph tags to customize link previews.
    Learn more: https://developers.facebook.com/docs/sharing/webmasters -->
  <meta property="og:url"           content="https://www.naver.com/" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="Your Website Title" />
  <meta property="og:description"   content="Your description" />
  <meta property="og:image"         content="https://s.pstatic.net/static/www/img/2018/sp_login_v180727.png" />
</head>
<body>

  <!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <!-- Your share button code -->
  <div class="fb-share-button"
    data-href="https://www.naver.com/"
    data-layout="button_count">
  </div>


  <button id="twitter">트위터</button>

  <script src="/assets/template/jquery.min.js"></script>
  <script>
    $('body').on('click', '#twitter', function() {
      var url = 'https://www.naver.com/';
      window.open('http://twitter.com/share?url=공유URL&text='+url);
    });
  </script>


</body>
</html>
