<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">

    <meta name="keywords" content="radon Dr.">
    <meta name="description" content="Global Total Radon Solution">

    <link href="/assets/images/favicon/favicon.png" rel="shortcut icon" type="image/x-icon">
    <link href="/assets/images/favicon/favicon.png" rel="icon" type="image/x-icon">

    <title>radon Dr.</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="/assets/css/global.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/qna.css" rel="stylesheet" type="text/css">
</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>
    <!-- Preloader_END -->

    <!-- Navigation -->
    <header>
        <nav class="navbar navbar-global navbar-fixed-top" role="navigation">
            <div class="container p0">
                <div class="logo">
                    <a href="/">
                        <img src="/assets/images/logo_w.png" title="로고" alt="로고"  />
                    </a>
                </div>

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="custom-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="/sub/radondr">라돈닥터</a>
                        </li>
                        <li>
                            <a href="/sub/solution">솔루션</a>
                        </li>
                        <li>
                            <a href="/sub/review">저감사례</a>
                        </li>
                        <li>
                            <a href="/sub/news">뉴스</a>
                        </li>
                        <li>
                            <a href="/sub/qna">Q&A</a>
                        </li>
                        <li>
                            <a href="/sub/contactus">문의하기</a>
                        </li>
                        <li>
                            <a href="http://www.geomall.kr/" target="_blank">쇼핑몰</a>
                        </li>
                    </ul>
                </div>
            </div><!-- Container_END -->
        </nav>
    </header>
    <!-- Navigation_END -->

    <!-- qna -->
    <section id="qna" class="wow fadeInUp">
        <div class="container-fluid">
            <div class="container">
                <div class="section-header">
                    <h3>Q & A</h3>
                </div>
            </div>
        </div>
        <div class="container-fluid p0">
            <div class="container mb80">
                <div class="vboard">
                    <div class="col-xs-12">
                        <div class="table-responsive col-xs-12 p0">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="width: 10%;">번호</th>
                                        <th style="width: 63%;">제&nbsp;목</th>
                                        <th style="width: 12%;">작성자</th>
                                        <th style="width: 15%;">등록일</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?foreach($list as $row) { $div = ''; $class = "";
                                    if($row->qna_reply) { $div = '<span>ㄴRe</span>'; $class = "reply";}?>
                                    <tr class = "<?=$class?>">
                                        <td><?=$row->num?></td>
                                        <td style="text-align: left;">
                                            <?=$div?>
                                            <a href="#">
                                                <p><?=$row->qna_text?></p>
                                            </a>
                                        </td>
                                        <td>
                                            <p><?=$row->qna_id?></p>
                                        </td>
                                        <td>
                                            <?$arr = explode(' ',$row->qna_date); $arrs = explode('-',$arr[0]); $date = implode('. ',$arrs);?>
                                            <p><?=$date?>.</p>
                                        </td>
                                    </tr>
                                  <?}?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="vboard-paging">
                    <div class="col-xs-12">
                        <div class="dataTables_paginate paging_bootstrap_number" id="sample_1_paginate">
                              <?echo $this->pagination->create_links();?>
                        </div>
                    </div>
                </div>

                <div class="reply_area">
                    <div class="board_reply_form">
                        <div class="reply_info">
                          <form>
                            <div class="left">
                                <input type="text" placeholder="아이디" name="qna_id" required="required" class="req" value="<?=$_SESSION['id']?>" <?if($_SESSION['id']){ echo "readonly";}?> />
                                <input type="password" placeholder="패스워드" name="qna_password" required="required" class="req" value="<?=$_SESSION['pass']?>" <?if($_SESSION['pass']) { echo "readonly";}?>/>
                            </div>
                            <div class="right">
                                <button type="button" id="qnago" class="btn">질문등록</button>
                                <button type="submit" class="btn" style="display:none;"></button>
                            </div>
                            <div class="reply_text">
                                <textarea cols="30" rows="3" class="req" name="qna_text" placeholder="질문쓰기" required="required"></textarea>
                            </div>
                          </form>
                        </div>
                    </div>
                </div>
            </div><!-- /.container -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- qna_END -->

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 p0">
                    <div class="foot_logo">
                        <img src="/assets/images/logo_w.png" title="로고" alt="로고">
                    </div>
                    <div class="foot_cont">
                        상호명 : C&H, Inc | 주소 : 서울특별시 강남구 강남대로 320, 황화빌딩 1505호<br>
                        대표전화 : (+82) 02-554-3869 | 팩스 : (+82) 02-556-0480 | 이메일 : radon@candh.co.kr
                        <span class="m_link">
                            <a href="http://www.newturntree.com/" target="_blank" title="뉴턴트리 홈페이지로 이동" alt="뉴턴트리 홈페이지로 이동">Newturn Tree</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Video</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Studio</a>
                        </span>
                    </div>
                    <div class="foot_contact">
                        <button onclick="location.href='/sub/contactus'" title="Contact Us로 이동" alt="Contact Us로 이동">
                            <i class="fal fa-paper-plane"></i>
                        </button>
                    </div>
                </div>
                <div class="col-xs-12 copyright">
                    <p>Copyright &copy; radon Dr. All rights reserved.</p>
                </div>
            </div><!-- row end -->
        </div><!-- Container_END -->
    </footer>

    <!-- Float-btn -->
    <div class="float-btn">
        <a href="/sub/qna">
            <img src="/assets/images/main/qna-icon-g.png" alt="">
        </a>
    </div>
    <!-- Float-btn_END-->

    <!-- Scroll to top -->
    <div class="scroll-up" title="TOP" alt="TOP">
        <a href="#qna"><span class="glyphicon glyphicon-menu-up"></span></a>
    </div>
    <!-- Scroll to top end-->

    <!-- Javascript files -->
    <script src="/assets/lib/sticky/jquery.sticky.js"></script>
    <script src="/assets/lib/wow/wow.min.js"></script>
    <script src="/assets/lib/isotope/isotope.pkgd.min.js"></script>
    <script src="/assets/lib/cbpViewModeSwitch.js"></script>
    <script src="/assets/lib/classie.js"></script>

    <script src="/assets/js/sub.js"></script>
    <script src="/assets/js/common.js"></script>
    <script src="/assets/js/radondrdev.js"></script>
</body>
</html>
