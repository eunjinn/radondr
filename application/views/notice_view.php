<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">

    <meta name="keywords" content="Radon Dr.">
    <meta name="description" content="Global Total Radon Solution">

    <link href="/assets/images/favicon/favicon.png" rel="shortcut icon" type="image/x-icon">
    <link href="/assets/images/favicon/favicon.png" rel="icon" type="image/x-icon">

    <title>Radon Dr.</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="/assets/css/global.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/notice_view.css" rel="stylesheet" type="text/css">
</head>
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>
    <!-- Preloader_END -->

    <!-- Navigation -->
    <header>
        <nav class="navbar navbar-global navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="logo">
                    <a href="/">
                        <img src="/assets/images/logo_w.png" title="로고" alt="로고"  />
                    </a>
                </div>

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="custom-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="/sub/radondr">Radon Dr.</a>
                        </li>
                        <li>
                            <a href="/sub/solution">Solution</a>
                        </li>
                        <li>
                            <a href="/sub/review">Review</a>
                        </li>
                        <li>
                            <a href="/sub/notice">Notice</a>
                        </li>
                        <li>
                            <a href="/sub/contactus">Contact Us</a>
                        </li>
                        <li>
                            <a href="http://www.geomall.kr/" target="_blank">Shop</a>
                        </li>
                    </ul>
                </div>
            </div><!-- Container_END -->
        </nav>
    </header>
    <!-- Navigation_END -->

    <!-- Notice_view -->
    <section id="notice_view" class="wow fadeInUp">
        <div class="container-fluid p0">
            <div class="container">
                <div class="col-xs-12 p0 pb80">
                    <div class="vboard-contents">
                        <div class="header">
                          <?$arr = explode(' ',$data->notice_date);
                          $arrs = explode('-',$arr[0]);
                          $date = implode('. ',$arrs);?>
                            <p><?=$data->admin_name?>&nbsp;&nbsp;|&nbsp;&nbsp;<?=$date?>.</p>
                            <h3><?=$data->notice_title?></h3>
                        </div>
                        <div class="contents">
                          <?=$data->notice_text?>
                        </div>
                        <div class="info">
                            <div class="hr"></div>
                            <div class="left">
                                <button class="btn locationbtns" data-idx="<?=$pre?>"><i class="fas fa-chevron-left"></i> 이전글</button>
                                <button class="btn locationbtns" data-idx="<?=$next?>">다음글 <i class="fas fa-chevron-right"></i></button>
                                <button class="btn" onclick="location.href='/sub/notice'">목록으로</button>
                            </div>
                            <div class="right">
                                <button id="show_reply" class="btn">댓글달기</button>
                            </div>
                        </div>

                        <div class="reply_area">
                          <?foreach($list as $row) { $class = ''; $div = ''; $divs='';
                            $arr = explode(' ',$row->noreply_date); $datearr = explode('-',$arr[0]); $date = implode('. ',$datearr); $timearr = explode(':',$arr[1]); $time = $timearr[0].':'.$timearr[1];
                            if($row->noreply_idx == $row->noreply_replyidx) { $class = "board_reply"; $divs = '<a>답글</a>'; } else { $class = "board_reply_reply"; $div = '<div class="re_icon">└</div>';}?>

                            <div class="<?=$class?>">
                              <?=$div?>
                                <div class="reply_info" data-idx="<?=$row->noreply_idx?>">
                                    <div class="left"><b><?=$row->noreply_id?></b><p>&nbsp;&nbsp;|&nbsp;&nbsp;<?=$date?>&nbsp;&nbsp;<?=$time?></p></div>
                                    <div class="right"><a class="noreplyedit">수정</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="noreplydel">삭제</a></div>
                                </div>
                                <div class="reply_text noreplyidx"><?=$row->noreply_text?><?=$divs?></div>
                            </div>
                          <?}?>
                            <div class="board_reply_form" style="display:none;">
                              <form>
                                <div class="reply_info">
                                    <div class="left">
                                        <input type="text" class="req" name="noreply_id" placeholder="아이디" required="required" value="<?=$_SESSION['id']?>" <?if($_SESSION['id']) { echo "readonly"; }?> />
                                        <input type="password" class="req" name="noreply_password" placeholder="패스워드" required="required" value="" />
                                    </div>
                                    <div class="right">
                                        <button type="button" class="btn" id="noreplygo">댓글등록</button>
                                        <button type="submit" style="display:none;"></button>
                                    </div>
                                    <div class="reply_text">
                                        <textarea cols="30" id="noreply_text" class="req" name="noreply_text" rows="3" placeholder="답글쓰기" required="required"></textarea>
                                    </div>
                                    <input type="hidden" name="noreply_board" value="<?=$data->notice_idx?>">
                                    <input type="hidden" id="noreplyidx" name="noreply_replyidx">
                                    <input type="hidden" id="noreply_idx" name="noreply_idx">
                                </div>
                              </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.container -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- Notice_view_END -->

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 p0">
                    <div class="foot_logo">
                        <img src="/assets/images/logo_w.png" title="로고" alt="로고">
                    </div>
                    <div class="foot_cont">
                        상호명 : C&H, Inc | 주소 : 서울특별시 강남구 강남대로 320, 황화빌딩 1505호<br>
                        대표전화 : (+82) 02-554-3869 | 팩스 : (+82) 02-556-0480 | 이메일 : radon@candh.co.kr
                        <span class="m_link">
                            <a href="http://www.newturntree.com/" target="_blank" title="뉴턴트리 홈페이지로 이동" alt="뉴턴트리 홈페이지로 이동">Newturn Tree</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Video</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Studio</a>
                        </span>
                    </div>
                    <div class="foot_contact">
                        <button onclick="location.href='./contactus.html'" title="Contact Us로 이동" alt="Contact Us로 이동">
                            <i class="fal fa-paper-plane"></i>
                        </button>
                    </div>
                </div>
                <div class="col-xs-12 copyright">
                    <p>Copyright &copy; Radon Dr. All rights reserved.</p>
                </div>
            </div><!-- row end -->
        </div><!-- Container_END -->
    </footer>

    <!-- Scroll to top -->
    <div class="scroll-up" title="TOP" alt="TOP">
        <a href="#notice_view"><span class="glyphicon glyphicon-menu-up"></span></a>
    </div>
    <!-- Scroll to top end-->

    <!-- Javascript files -->
    <script src="/assets/lib/wow/wow.min.js"></script>
    <script src="/assets/lib/isotope/isotope.pkgd.min.js"></script>
    <script src="/assets/lib/cbpViewModeSwitch.js"></script>
    <script src="/assets/lib/classie.js"></script>

    <script src="/assets/js/sub.js"></script>
    <script src="/assets/js/common.js"></script>
    <script src="/assets/js/radondrdev.js"></script>
</body>
</html>
