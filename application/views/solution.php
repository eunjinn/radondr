<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">

    <meta name="keywords" content="radon Dr.">
    <meta name="description" content="Global Total Radon Solution">

    <link href="/assets/images/favicon/favicon.png" rel="shortcut icon" type="image/x-icon">
    <link href="/assets/images/favicon/favicon.png" rel="icon" type="image/x-icon">

    <title>radon Dr.</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/lib/animate/animate.css" rel="stylesheet" type="text/css">

    <link href="/assets/css/global.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/sub.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/solution.css" rel="stylesheet" type="text/css">
</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>
    <!-- Preloader_END -->

    <!-- Navigation -->
    <header>
        <nav class="navbar navbar-global navbar-fixed-top" role="navigation">
            <div class="container p0">
                <div class="logo">
                    <a href="/">
                        <img src="/assets//images/logo_w.png" title="로고" alt="로고"  />
                    </a>
                </div>

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="custom-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="/sub/radondr">라돈닥터</a>
                        </li>
                        <li>
                            <a href="/sub/solution">솔루션</a>
                        </li>
                        <li>
                            <a href="/sub/review">저감사례</a>
                        </li>
                        <li>
                            <a href="/sub/news">뉴스</a>
                        </li>
                        <li>
                            <a href="/sub/qna">Q&A</a>
                        </li>
                        <li>
                            <a href="/sub/contactus">문의하기</a>
                        </li>
                        <li>
                            <a href="http://www.geomall.kr/" target="_blank">쇼핑몰</a>
                        </li>
                    </ul>
                </div>
            </div><!-- Container_END -->
        </nav>
    </header>
    <!-- Navigation_END -->

    <!-- Banner start -->
    <section id="sub-bg">
        <div class="contaier-fluid">
            <div class="subpage-banner">
                <div class="banner-cover">
                    <div class="container">
                        <div class="row wow fadeInLeft">
                            <p>Total Radon Solution</p>
                            <p>R.D Solution</p>
                            <p>
                                각각의 솔루션은 데이터 분석 및 컨설팅, 전문 시공, 유해성 관리로 구성되어 있으며 <br>
                                고객의 선택에 따라 다양한 단계의 솔루션을 제공 받으실 수 있습니다.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="banner-background solution"></div>
                <div class="container banner-img">
                    <div class="solution">
                        <img class="wow fadeInDown" src="/assets/images/solution/solution.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Banner_END -->

    <!-- Solution -->
    <section id="solution" class="sub-position wow fadeInUp">
        <div class="container">
            <div class="section-header mt120">
                <p>국내 토탈 라돈 솔루션</p>
                <h3>radon Dr. Solution for Korea</h3>
            </div>
            <div class="row solution">
                <div class="sol_1 col-xs-12">
                    <p>
                        라돈 닥터가 제공하는 토탈 라돈 솔루션은 총 3가지 형태로써<br><br>
                        라돈 저감 솔루션(Radon Mitigation Solution) 기반의 radon Dr. MS,<br>
                        라돈 방호 솔루션(Anti-Radon Solution) 기반의 radon Dr. RDR,<br>
                        지하수 라돈 저감 솔루션(Ground Water Radon Reduction Solution) 기반의 radon Dr. GW 로 구성되어 있습니다.<br><br>

                        각각의 솔루션은 데이터 분석 및 컨설팅, 전문 시공, 유해성 관리로 구성되어 있으며 고객의 선택에 따라 다양한 단계의 솔루션을 제공 받으실 수 있습니다.
                    </p>
                </div>
                <div class="sol_2">
                    <div class="col-sm-4 col-xs-12 wow fadeInRight p0">
                        <a href="/sub/solution_ms">
                            <div class="box-img">
                                <img src="/assets/images/product/1.png" title="홈페이지 페이지로 이동" alt="홈페이지 페이지로 이동">
                                <p>radon Dr. MS</p>
                                <p>라돈 저감 솔루션(Radon Mitigation Solution)</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4 col-xs-12 wow fadeInUp p0">
                        <a href="/sub/solution_rdr">
                            <div class="box-img">
                                <img src="/assets/images/product/2.png" title="쇼핑몰 페이지로 이동" alt="쇼핑몰 페이지로 이동">
                                <p>radon Dr. RDR</p>
                                <p>라돈 방호 솔루션(Anti-Radon Solution)</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4 col-xs-12 wow fadeInLeft p0">
                        <a href="/sub/solution_gw">
                            <div class="box-img">
                                <img src="/assets/images/product/3.png" title="모바일/앱 페이지로 이동" alt="모바일/앱 페이지로 이동">
                                <p>radon Dr. GW</p>
                                <p>지하수 라돈 저감 솔루션(Ground Water Radon Reduction Solution)</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Solution_END -->

    <!-- USA -->
    <section id="usa" class="wow fadeInUp">
        <div class="container">
            <div class="section-header">
                <p>미국 토탈 라돈 솔루션</p>
                <h3>radon Dr. Solution for USA</h3>
            </div>
            <div class="row usa">
                <div class="col-xs-12">
                    <p>
                        We provide Radon detecting services; Radon mitigation services, radon consulting services related to radon mitigation in NY, NJ, CT. For detailed information, please contact at 1-844-radondr(723-6637) or email steve@luxesseinc.com
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- USA_END -->

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 p0">
                    <div class="foot_logo">
                        <img src="/assets/images/logo_w.png" title="로고" alt="로고">
                    </div>
                    <div class="foot_cont">
                        상호명 : C&H, Inc | 주소 : 서울특별시 강남구 강남대로 320, 황화빌딩 1505호<br>
                        대표전화 : (+82) 02-554-3869 | 팩스 : (+82) 02-556-0480 | 이메일 : radon@candh.co.kr
                        <span class="m_link">
                            <a href="http://www.newturntree.com/" target="_blank" title="뉴턴트리 홈페이지로 이동" alt="뉴턴트리 홈페이지로 이동">Newturn Tree</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Video</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Studio</a>
                        </span>
                    </div>
                    <div class="foot_contact">
                        <button onclick="location.href='/sub/contactus'" title="Contact Us로 이동" alt="Contact Us로 이동">
                            <i class="fal fa-paper-plane"></i>
                        </button>
                    </div>
                </div>
                <div class="col-xs-12 copyright">
                    <p>Copyright &copy; radon Dr. All rights reserved.</p>
                </div>
            </div><!-- row end -->
        </div><!-- Container_END -->
    </footer>

    <!-- Float-btn -->
    <div class="float-btn">
        <a href="/sub/qna">
            <img src="/assets/images/main/qna-icon-g.png" alt="">
        </a>
    </div>
    <!-- Float-btn_END-->

    <!-- Scroll to top -->
    <div class="scroll-up" title="TOP" alt="TOP">
        <a href="#sub-bg"><span class="glyphicon glyphicon-menu-up"></span></a>
    </div>
    <!-- Scroll to top end-->

    <!-- Javascript files -->
    <script src="/assets/lib/wow/wow.min.js"></script>
    <script src="/assets/lib/isotope/isotope.pkgd.min.js"></script>

    <script src="/assets/js/sub.js"></script>
</body>
</html>
