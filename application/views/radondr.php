<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">

    <meta name="keywords" content="radon Dr.">
    <meta name="description" content="Global Total Radon Solution">

    <link href="/assets/images/favicon/favicon.png" rel="shortcut icon" type="image/x-icon">
    <link href="/assets/images/favicon/favicon.png" rel="icon" type="image/x-icon">

    <title>radon Dr.</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/lib/animate/animate.css" rel="stylesheet" type="text/css">

    <link href="/assets/css/global.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/sub.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/radondr.css" rel="stylesheet" type="text/css">
</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>
    <!-- Preloader_END -->

    <!-- Navigation -->
    <header>
        <nav class="navbar navbar-global navbar-fixed-top" role="navigation">
            <div class="container p0">
                <div class="logo">
                    <a href="/">
                        <img src="/assets/images/logo_w.png" title="로고" alt="로고"  />
                    </a>
                </div>

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="custom-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="/sub/radondr">라돈닥터</a>
                        </li>
                        <li>
                            <a href="/sub/solution">솔루션</a>
                        </li>
                        <li>
                            <a href="/sub/review">저감사례</a>
                        </li>
                        <li>
                            <a href="/sub/news">뉴스</a>
                        </li>
                        <li>
                            <a href="/sub/qna">Q&A</a>
                        </li>
                        <li>
                            <a href="/sub/contactus">문의하기</a>
                        </li>
                        <li>
                            <a href="http://www.geomall.kr/" target="_blank">쇼핑몰</a>
                        </li>
                    </ul>
                </div>
            </div><!-- Container_END -->
        </nav>
    </header>
    <!-- Navigation_END -->

    <!-- Banner start -->
    <section id="sub-bg">
        <div class="contaier-fluid">
            <div class="subpage-banner">
                <div class="banner-cover">
                    <div class="container">
                        <div class="row wow fadeInLeft">
                            <p>Since 2007.</p>
                            <p>radon Dr.</p>
                            <p>
                                글로벌 스탠다드 전문성과 풍부한 현장경험을 겸비한 라돈 전문 브랜드.<br>
                                라돈으로 인한 문제는 라돈 전문가 라돈닥터에게 맡겨주세요.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="banner-background radondr"></div>
                <div class="container banner-img">
                    <div class="radondr">
                        <img class="wow fadeInUp" src="/assets/images/radondr/doctors.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Banner_END -->

    <!-- Brand -->
    <section id="brand" class="sub-position wow fadeInUp">
        <div class="container">
            <div class="section-header mt120">
                <p>라돈 전문 브랜드</p>
                <h3>radon Dr.</h3>
            </div>
            <div class="row brand">
                <div class="col-xs-12">
                    <p>
                        씨앤에치아이앤씨의 토탈 라돈 솔루션 브랜드 라돈 닥터는 2007년 이래로 실내주거환경을 라돈으로 부터 안전하게 지키기 위한 노력을 경주하고 있습니다. 라돈 닥터는 미 환경청(EPA)가 공인한 NRPP 자격증을 취득함으로써 전문성을 인정 받았고, 국내외(대한민국, 미국등) 주거시설 및 공공시설, 라돈 컨설팅, 저감 시공, 유해성 관리를 수행함으로써 풍부한 현장경험을 확보하였습니다. 대한민국 실내주거환경의 질을 높이기 위해 라돈 전문 브랜드 라돈 닥터는 최선을 다하고 있습니다.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- Brand_END -->

    <!-- Tech -->
    <section id="tech" class="wow fadeInUp">
        <div class="container">
            <div class="section-header">
                <h3>핵심보유기술</h3>
            </div>
            <div class="row tech">
                <div class="tech_1 col-md-5 col-xs-12">
                    <p>
                        <b>ISO 9001 품질 경영 시스템 인증 획득</b><br>
                        라돈 저감 관련 업계 최초
                    </p>
                    <img src="/assets/images/radondr/tech2.jpg" alt="">
                </div>
                <div class="tech_2 col-md-7 col-xs-12">
                    <table class="table table-bordered">
                        <thead class="thead-gray">
                            <tr>
                                <th>기술명(특허명)</th>
                                <th>특허번호</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>라돈 제거 시스템</td>
                                <td>10-1543757</td>
                            </tr>
                            <tr>
                                <td>건축 실내에 유입되는 라돈가스를 저감하는 장치</td>
                                <td>10-1027076</td>
                            </tr>
                            <tr>
                                <td>대용량 지하수 라돈 저감 장치</td>
                                <td>10-1151633</td>
                            </tr>
                            <tr>
                                <td>소규모 수역의 수질정화 시스템</td>
                                <td>10-1279137</td>
                            </tr>
                            <tr>
                                <td>수위감지기 및 이를 이용한 지하수 계측장치</td>
                                <td>10-0810116</td>
                            </tr>
                            <tr>
                                <td>정화 장치</td>
                                <td>10-1010026</td>
                            </tr>
                            <tr>
                                <td>지하수 관정 청소 장치 및 이를 이용한 청소공법</td>
                                <td>10-0651052</td>
                            </tr>
                            <tr>
                                <td>지하수 라돈 저감 장치</td>
                                <td>10-0911416</td>
                            </tr>
                            <tr>
                                <td>지하수 수질 관리 시스템</td>
                                <td>10-1086049</td>
                            </tr>
                            <tr>
                                <td>지하수 정화 시스템</td>
                                <td>10-1118510</td>
                            </tr>
                            <tr>
                                <td>토양가스를 이용한 실시간 자동라돈 모니터링 시스템 및 그 방법</td>
                                <td>10-1040072</td>
                            </tr>
                            <tr>
                                <td>토양의 라돈 위험도 측정시스템 및 그 측정방법</td>
                                <td>10-1443794</td>
                            </tr>
                            <tr>
                                <td>회전형 가동보 시스템</td>
                                <td>10-1135181</td>
                            </tr>
                            <tr>
                                <td>라돈저감장치 설계 적합도 선정시스템과 그 선정방법</td>
                                <td>10-1606552</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- Tech_END -->

    <!-- Certification -->
    <section id="certification" class="wow fadeInUp">
        <div class="container">
            <div class="section-header">
                <h3>NRPP Certification</h3>
            </div>
            <div class="row certi">
                <div class="certi_1 col-xs-12">
                    <p>
                        미 환경청(EPA)은 1995년이래로 라돈관련 서비스(측정, 저감)제공자의 숙련도를 평가하는 프로그램(Radon Proficiency Prigrams)의 시행 을 통해 자격증 제도가 보편화 하고 있었습니다. 또한, 미 전역을 4개 권역르로 나뉘어 , 지역별 라돈 교육센터를 설립하여 강좌를 개설 하여 왔습니다. 이후 EPA에서는 RPP를 폐지하고, 대신 비영리 민간기구에 위탁을 하면서, NRPP가 기존의 RPP프로그램과 동등하다는 것을 공식 인증 하였습니다. NRPP는 현재, AARST(American Association of Radon Scientists and Technologists)에서 운영하고 있습니다. 대부분의 주에서 NRPP자격증을 인정하며, 라돈농도가 특히 높은 일부 주 (NJ, PA등)에서는 NRPP자격증이외에, 주 정부차원에서 별도의 자격증 시험 통과를 의무화하고 있습니다.
                    </p>
                </div>
                <div class="certi_2 col-xs-12">
                    <table class="table table-bordered">
                        <thead class="thead-gray">
                            <tr>
                                <th>라돈측정전문가(Residential Measurement Provider)</th>
                                <th>라돈저감 시공 전문가(Residential Mitigration Provider)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Physics</td>
                                <td>diagnostics & general ASD</td>
                            </tr>
                            <tr>
                                <td>Health Effects</td>
                                <td>treating crawl spaces</td>
                            </tr>
                            <tr>
                                <td>Radon Entry</td>
                                <td>tile treatng sub slabs treatng block walls HRV's & Ventileation Alternative Approaches RMS & Ethics </td>
                            </tr>
                            <tr>
                                <td>Conducting Measurement Interpret Non Real Estimate Interpret Real Estimate QA/QC</td>
                                <td>Radon in Water Worker & Client Safety Radon Entry </td>
                            </tr>
                            <tr>
                                <td>Passive Devices</td>
                                <td>Physics </td>
                            </tr>
                            <tr>
                                <td>Active Devices</td>
                                <td>Measurements </td>
                            </tr>
                            <tr>
                                <td>Basic Mitigation Approach Radon in Water</td>
                                <td>Health Effects </td>
                            </tr>
                            <tr>
                                <td>Standards and Conduct</td>
                                <td>Stealing </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>Piping and Fans </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>RRNC</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="certi_3">
                    <div class="col-xs-4">
                        <img src="/assets/images/radondr/certi1.jpg" alt="">
                    </div>
                    <div class="col-xs-4">
                        <img src="/assets/images/radondr/certi2.jpg" alt="">
                    </div>
                    <div class="col-xs-4">
                        <img src="/assets/images/radondr/certi3.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Certification_END -->

    <!-- Freezone -->
    <section id="freezone" class="wow fadeInUp">
        <div class="container">
            <div class="section-header">
                <h3>Radon Free Zone</h3>
            </div>
            <div class="row free">
                <div class="col-xs-12">
                    <p>
                        라돈닥터 토탈 라돈 솔루션이 제공된 공간에 대해서는 2가지 단계로 관리되어 인증됩니다. 먼저 라돈닥터 멤버스 임을 알리는 마크를 부착하여 자연 방사성 물질인 라돈으로 부터 안전하게 관리되고 있는 장소임을 알려드리고, 라돈이 가장 높게 나타나는 겨울을 포함하여 1년간 라돈에 대한 위험성을 검토한 후 최종적으로 Radon Free Zone이라는 공간에 대한 인증 마크를 제공합니다. 라돈닥터 멤버스 및 Radon Free Zone은 해당 공간을 이용하는 모든 이에게 라돈에 대한 믿음과 안심을 약속드립니다.
                    </p>
                </div>
                <div class="col-xs-12 text-center">
                    <img src="/assets/images/radondr/radon-free.jpg" alt="">
                </div>
            </div>
        </div>
    </section>
    <!-- Tech_END -->

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 p0">
                    <div class="foot_logo">
                        <img src="/assets/images/logo_w.png" title="로고" alt="로고">
                    </div>
                    <div class="foot_cont">
                        상호명 : C&H, Inc | 주소 : 서울특별시 강남구 강남대로 320, 황화빌딩 1505호<br>
                        대표전화 : (+82) 02-554-3869 | 팩스 : (+82) 02-556-0480 | 이메일 : radon@candh.co.kr
                        <span class="m_link">
                            <a href="http://www.newturntree.com/" target="_blank" title="뉴턴트리 홈페이지로 이동" alt="뉴턴트리 홈페이지로 이동">Newturn Tree</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Video</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Studio</a>
                        </span>
                    </div>
                    <div class="foot_contact">
                        <button onclick="location.href='/sub/contactus'" title="Contact Us로 이동" alt="Contact Us로 이동">
                            <i class="fal fa-paper-plane"></i>
                        </button>
                    </div>
                </div>
                <div class="col-xs-12 copyright">
                    <p>Copyright &copy; radon Dr. All rights reserved.</p>
                </div>
            </div><!-- row end -->
        </div><!-- Container_END -->
    </footer>

    <!-- Float-btn -->
    <div class="float-btn">
        <a href="/sub/qna">
            <img src="/assets/images/main/qna-icon-g.png" alt="">
        </a>
    </div>
    <!-- Float-btn_END-->

    <!-- Scroll to top -->
    <div class="scroll-up" title="TOP" alt="TOP">
        <a href="#sub-bg"><span class="glyphicon glyphicon-menu-up"></span></a>
    </div>
    <!-- Scroll to top end-->

    <!-- Javascript files -->
    <script src="/assets/lib/wow/wow.min.js"></script>
    <script src="/assets/lib/isotope/isotope.pkgd.min.js"></script>

    <script src="/assets/js/sub.js"></script>
</body>
</html>
