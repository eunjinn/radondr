<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
	<meta name="format-detection" content="telephone=no">

	<meta name="keywords" content="radon Dr.">
	<meta name="description" content="Global Total Radon Solution">

	<link href="/assets/images/favicon/favicon.png" rel="shortcut icon" type="image/x-icon">
	<link href="/assets/images/favicon/favicon.png" rel="icon" type="image/x-icon">

	<title>radon Dr.</title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

	<link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/lib/full-page-scroll/full-page-scroll.css" rel="stylesheet" type="text/css">

	<link href="/assets/css/global.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/main.css" rel="stylesheet" type="text/css">
</head>
<body>

	<!-- Preloader -->
	<div id="preloader">
		<div id="status"></div>
	</div>
	<!-- Preloader_END -->

	<!-- Navigation -->
	<header>
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="container p0">
				<div class="logo">
					<a href="/">
						<img src="/assets/images/logo_w.png" title="로고" alt="로고"  />
					</a>
				</div>

				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<div class="collapse navbar-collapse" id="custom-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="/sub/radondr">라돈닥터</a>
						</li>
						<li>
							<a href="/sub/solution">솔루션</a>
						</li>
						<li>
							<a href="/sub/review">저감사례</a>
						</li>
						<li>
							<a href="/sub/news">뉴스</a>
						</li>
						<li>
							<a href="/sub/qna">Q&A</a>
						</li>
						<li>
							<a href="/sub/contactus">문의하기</a>
						</li>
						<li>
							<a href="http://www.geomall.kr/" target="_blank">쇼핑몰</a>
						</li>
					</ul>
				</div>
			</div><!-- Container_END -->
		</nav>
	</header>
	<!-- Navigation_END -->

	<!-- Main -->
	<div id="main" class="scroll-container">
		<section id="sect1">
			<div class="cover">
				<div class="area">
					<div class="contents">
						<div class="left">
							<div class="cont-img">
								<img src="/assets/images/main/1page_img.png">
							</div>
						</div>
						<div class="right">
							<div class="title">
								<img src="/assets/images/main/1page_text.png">
							</div>
							<p class="text">
								1급 발암 물질 라돈 측정, 평가, 저감, 설계, 시공, 관리 분야까지<br>
								라돈닥터만의 최고의 기술을 제공합니다.
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="sect2">
			<div class="cover">
				<div class="area">
					<div class="contents">
						<div class="left">
							<div class="point">
								<img src="/assets/images/main/radon-icon.png">
							</div>
							<div class="title">
								<img src="/assets/images/main/2page_text.png">
							</div>
							<p class="text">
								라돈은 언제 어디에서나 존재하는<br>
								무색, 무취, 무미의 기체인 자연방사능 물질입니다.
							</p>
						</div>
						<div class="right">
							<div class="cont-img">
								<img src="/assets/images/main/2page_img.png">
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="sect3">
			<div class="cover">
				<div class="area">
					<div class="contents">
						<div class="left">
							<div class="cont-img">
								<img src="/assets/images/main/3page_img.png">
							</div>
						</div>
						<div class="right">
							<div class="title">
								<img src="/assets/images/main/3page_text.png">
							</div>
							<p class="text">
								라돈닥터는 글로벌 스탠다드 전문성과<br>
								풍부한 현장 경험을 겸비한 라돈 전문 브랜드 입니다.
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="sect4">
			<div class="cover">
				<div class="area">
					<div class="contents">
						<div class="left">
							<div class="title">
								<img src="/assets/images/main/4page_text.png">
							</div>
							<p class="text">
								라돈닥터는 고객의 상황에 따라 다양한 단계의 솔루션을 제공합니다.
							</p>
							<div class="solutions clearfix wow fadeInUp">
								<div class="col-sm-4 p0">
									<a href="/sub/solution_ms">
										<div class="box-img">
											<img src="/assets/images/product/1.png" title="홈페이지 페이지로 이동" alt="홈페이지 페이지로 이동">
											<p>radon Dr. MS</p>
											<p>라돈 저감 솔루션</p>
										</div>
									</a>
								</div>
								<div class="col-sm-4 p0">
									<a href="/sub/solution_rdr">
										<div class="box-img">
											<img src="/assets/images/product/2.png" title="쇼핑몰 페이지로 이동" alt="쇼핑몰 페이지로 이동">
											<p>radon Dr. RDR</p>
											<p>라돈 방호 솔루션</p>
										</div>
									</a>
								</div>
								<div class="col-sm-4 p0">
									<a href="/sub/solution_gw">
										<div class="box-img">
											<img src="/assets/images/product/3.png" title="모바일/앱 페이지로 이동" alt="모바일/앱 페이지로 이동">
											<p>radon Dr. GW</p>
											<p>지하수 라돈 저감 솔루션</p>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="back_img">
					<img src="/assets/images/main/4page_img.png">
				</div>
			</div>
		</section>
		<section id="sect5">
			<div class="cover">
				<div class="area">
					<div class="footer">
						<div class="col-md-6">
							<div class="foot_cont">
								<p>
									Adress : 서울특별시 강남구 강남대로 320, 황화빌딩 1505호 <br>
									Email : radon@candh.co.kr <br>
									Phone : (+82) 02-554-3869 <br>
									Fax : (+82) 02-556-0480 <br>
								</p>
								<p>
									C&H, Inc의 브랜드인 <b>Radon Dr</b>는 Radon과 Doctor의 합성어로 라돈 측정, 설계, 시공, 평가, 관리 분야까지 최고의 네트워크를 구축하고 있습니다.
								</p>
								<div class="sns">
									<a href="https://www.facebook.com/radondr.co.kr" target="_blank">
										<i class="fab fa-facebook-f"></i>
									</a>
									<a href="https://www.youtube.com/channel/UCDSIDaX3sQun12e59F8JSyQ" target="_blank">
										<i class="fab fa-youtube"></i>
									</a>
									<a href="http://blog.naver.com/dryoungpro" target="_blank">
										<i class="fas fa-rss-square"></i>
									</a>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="foot_logo">
								<img src="/assets/images/logo_w.png" title="로고" alt="로고"  />
							</div>
						</div>

					</div>
				</div>
			</div>
		</section>
	</div>
	<!-- Main_END -->

	<!-- Float-btn -->
	<div class="float-btn">
		<a href="/sub/qna">
			<img src="/assets/images/main/qna-icon.png" alt="">
		</a>
	</div>
	<!-- Float-btn_END-->

	<!-- Scroll to top -->
	<div class="scroll-up" title="TOP으로 이동" alt="TOP으로 이동">
		<a href="#home"><span class="glyphicon glyphicon-menu-up"></span></a>
	</div>
	<!-- Scroll to top_END-->

	<!-- Javascript files -->
	<script src="/assets/lib/full-page-scroll/full-page-scroll.js"></script>
	<script src="/assets/js/main.js"></script>

	<script>
		$('.panel-heading').click(function() {
			$('.panel-heading').removeClass('active');
			if(!$(this).closest('.panel').find('.panel-collapse').hasClass('in'))
				$(this).parents('.panel-heading').addClass('active');
		});
	</script>

	<!-- Contact Form JavaScript File -->
	<script src="/assets/contactform/contactform.js"></script>
</body>
</html>
