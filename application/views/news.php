<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">

    <meta name="keywords" content="radon Dr.">
    <meta name="description" content="Global Total Radon Solution">

    <link href="/assets/images/favicon/favicon.png" rel="shortcut icon" type="image/x-icon">
    <link href="/assets/images/favicon/favicon.png" rel="icon" type="image/x-icon">

    <title>radon Dr.</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="/assets/css/global.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/review.css" rel="stylesheet" type="text/css">
</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>
    <!-- Preloader_END -->

    <!-- Navigation -->
    <header>
        <nav class="navbar navbar-global navbar-fixed-top" role="navigation">
            <div class="container p0">
                <div class="logo">
                    <a href="/">
                        <img src="/assets/images/logo_w.png" title="로고" alt="로고"  />
                    </a>
                </div>

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="custom-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="/sub/radondr">라돈닥터</a>
                        </li>
                        <li>
                            <a href="/sub/solution">솔루션</a>
                        </li>
                        <li>
                            <a href="/sub/review">저감사례</a>
                        </li>
                        <li>
                            <a href="/sub/news">뉴스</a>
                        </li>
                        <li>
                            <a href="/sub/qna">Q&A</a>
                        </li>
                        <li>
                            <a href="/sub/contactus">문의하기</a>
                        </li>
                        <li>
                            <a href="http://www.geomall.kr/" target="_blank">쇼핑몰</a>
                        </li>
                    </ul>
                </div>
            </div><!-- Container_END -->
        </nav>
    </header>
    <!-- Navigation_END -->

    <!-- Review -->
    <section id="review" class="wow fadeInUp">
        <div class="container-fluid">
            <div class="container">
                <div class="section-header">
                    <h3>radon Dr. News</h3>
                </div>
            </div>
        </div>
        <div id="cbp-vm" class="cbp-vm-view-grid">

            <!-- Option -->
            <div class="container-fluid box_border p0">
                <div class="container">
                    <div id="option-wrap" class="col-xs-12">
                        <div class="option-area col-lg-8 col-md-6 col-sm-4 col-xs-12 p0">
                            <div class="cbp-vm-options btn-group">
                                <a href="#" class="btn cbp-vm-selected" data-view="cbp-vm-view-grid" title="썸네일형" alt="썸네일형">
                                    <span class="glyphicon glyphicon-th"></span>
                                </a>
                                <a href="#" class="btn" data-view="cbp-vm-view-list" title="리스트형" alt="리스트형">
                                    <span class="glyphicon glyphicon-th-list"></span>
                                </a>
                            </div>
                        </div>
                        <div class="search-area col-lg-4 col-md-6 col-sm-8 col-xs-12 p0">
                          <div class="input-group">
                              <input id="newsword" class="form-control" title="검색" type="text" placeholder="키워드로 찾아보세요" value="">
                              <div class="input-group-btn">
                                  <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search" id="newssearch"></span></button>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Option_END -->

            <!-- Grid & List -->
            <div class="container-fluid p0">
                <div class="container p0">
                    <ul class="cbp-list col-xs-12">

                      <?foreach($list as $row) { $class = '';?>
                        <li>
                            <div class="list-img">
                                <a href="/sub/news_view/<?=$row->news_idx?>">
                                    <div style="background-image:url('/assets/uploads/<?=$row->news_thumb?>');"></div>
                                </a>
                            </div>
                            <div class="list-contents">
                                <div class="title">
                                    <a href="/sub/news_view/<?=$row->news_idx?>"><?=$row->news_title?></a>
                                </div>
                                <p class="text" style=" width: 330px; overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 3; -webkit-box-orient: vertical;">
                                    <?=strip_tags($row->news_text)?>
                                </p>
                                <div class="hr"></div>
                                <div class="list-info">
                                    <div class="writer">
                                        <img src="/assets/images/sample.png" class="img-circle info-logo">
                                        <span><?=$row->admin_name?></span>
                                    </div>
                                    <div class="sns">
                                        <a class="newslike like" data-idx="<?=$row->news_idx?>"></a>
                                        <span><?=$row->news_like?></span>
                                        <a href="#" class="reply"></a>
                                        <span><?=$row->replycount?></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                      <?}?>

                    </ul>
                </div>

                <!-- More -->
                <div class="container">
                    <div class="btn more" data-now = "1" data-all="<?=$count?>" <?if($count == 1) { echo "style='display:none'"; }?>>
                        <h3>더보기 ▼</h3>
                    </div>
                </div>
                <!-- /.More -->
            </div><!-- /.container-fluid -->
        </div><!-- /.cbp-vm-switcher -->
    </section>
    <!-- Review_END -->

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 p0">
                    <div class="foot_logo">
                        <img src="/assets/images/logo_w.png" title="로고" alt="로고">
                    </div>
                    <div class="foot_cont">
                        상호명 : C&H, Inc | 주소 : 서울특별시 강남구 강남대로 320, 황화빌딩 1505호<br>
                        대표전화 : (+82) 02-554-3869 | 팩스 : (+82) 02-556-0480 | 이메일 : radon@candh.co.kr
                        <span class="m_link">
                            <a href="http://www.newturntree.com/" target="_blank" title="뉴턴트리 홈페이지로 이동" alt="뉴턴트리 홈페이지로 이동">Newturn Tree</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Video</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Studio</a>
                        </span>
                    </div>
                    <div class="foot_contact">
                        <button onclick="location.href='/sub/contactus'" title="Contact Us로 이동" alt="Contact Us로 이동">
                            <i class="fal fa-paper-plane"></i>
                        </button>
                    </div>
                </div>
                <div class="col-xs-12 copyright">
                    <p>Copyright &copy; radon Dr. All rights reserved.</p>
                </div>
            </div><!-- row end -->
        </div><!-- Container_END -->
    </footer>

    <!-- Float-btn -->
    <div class="float-btn">
        <a href="/sub/qna">
            <img src="/assets/images/main/qna-icon-g.png" alt="">
        </a>
    </div>
    <!-- Float-btn_END-->

    <!-- Scroll to top -->
    <div class="scroll-up" title="TOP" alt="TOP">
        <a href="#review"><span class="glyphicon glyphicon-menu-up"></span></a>
    </div>
    <!-- Scroll to top end-->

    <!-- Javascript files -->
    <script src="/assets/lib/sticky/jquery.sticky.js"></script>
    <script src="/assets/lib/wow/wow.min.js"></script>
    <script src="/assets/lib/isotope/isotope.pkgd.min.js"></script>
    <script src="/assets/lib/cbpViewModeSwitch.js"></script>
    <script src="/assets/lib/classie.js"></script>

    <script src="/assets/js/sub.js"></script>
    <script src="/assets/js/common.js"></script>
    <script src="/assets/js/radondrdev.js"></script>
</body>
</html>
