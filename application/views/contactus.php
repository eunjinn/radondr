<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">

    <meta name="keywords" content="radon Dr.">
    <meta name="description" content="Global Total Radon Solution">

    <link href="/assets/images/favicon/favicon.png" rel="shortcut icon" type="image/x-icon">
    <link href="/assets/images/favicon/favicon.png" rel="icon" type="image/x-icon">

    <title>radon Dr.</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="/assets/css/global.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/contactus.css" rel="stylesheet" type="text/css">
</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>
    <!-- Preloader_END -->

    <!-- Navigation -->
    <header>
        <nav class="navbar navbar-global navbar-fixed-top" role="navigation">
            <div class="container p0">
                <div class="logo">
                    <a href="/">
                        <img src="/assets/images/logo_w.png" title="로고" alt="로고"  />
                    </a>
                </div>

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="custom-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="/sub/radondr">라돈닥터</a>
                        </li>
                        <li>
                            <a href="/sub/solution">솔루션</a>
                        </li>
                        <li>
                            <a href="/sub/review">저감사례</a>
                        </li>
                        <li>
                            <a href="/sub/news">뉴스</a>
                        </li>
                        <li>
                            <a href="/sub/qna">Q&A</a>
                        </li>
                        <li>
                            <a href="/sub/contactus">문의하기</a>
                        </li>
                        <li>
                            <a href="http://www.geomall.kr/" target="_blank">쇼핑몰</a>
                        </li>
                    </ul>
                </div>
            </div><!-- Container_END -->
        </nav>
    </header>
    <!-- Navigation_END -->

    <!-- Contact Us-->
    <section id="contactus" class="wow fadeInUp">
        <div class="container">
            <div class="section-header">
                <h3>Contact Us</h3>
                <p>
                    radon Dr.는 고객님의 이야기에 언제나 귀를 열고 있습니다.<br>
                    문의남겨주시면 신속히 연락드리겠습니다.
                </p>
            </div>
            <form method="post" action="/consult/request/process.php" name="requestForm" id="requestForm" enctype="multipart/form-data" onsubmit="return CheckForm(this);">
                <input type="hidden" name="Mode" id="Mode" value="Request">
                <input type="hidden" name="url" value="">

                <h4 class="contact-title">개인정보처리방침</h4>
                <div class="scroll">
                    <div>
                        <b>1. 개인정보의 수집 및 이용 목적</b><br>
                        radon Dr.(이하 회사) 회사는 다음과 같은 목적을 위해 개인정보를 수집하고 이용합니다.<br>
                        <span>- 서비스 제공, 콘텐츠 제공, 웹 컨설팅 기초 자료 확보, 마케팅 활용, 상담 신청 처리, 구매 대행 및 요금 결제 등을 위한 원활한 의사소통 경로 확보</span><br><br>

                        <b>2. 수집하는 개인정보의 항목 및 수집방법</b><br>
                        회사는 다음과 같은 항목들을 수집하여 처리합니다.<br>
                        <span>- 필수항목 : 이름(업체명), 연락처, 이메일</span><br><br>

                        <b>3. 개인정보의 보유 및 이용기간</b><br>
                        회사는 원칙적으로 이용자의 개인정보 수집 및 이용목적이 달성되면 지체 없이 파기합니다.<br>
                        관계법령의 규정에 의하여 보존할 필요가 있는 경우에는 일정기간 동안 보존합니다.<br>
                        <span>- 문의하기 : 1년(정보통신망법)<br>
                        - 홈페이지 방문에 관한 기록 : 3개월(통신비밀보호법)<br>
                        - 소비자의 불만 또는 분쟁처리에 관한 기록 : 3년(전자상거래 등에서의 소비자 보호에 관한 법률)</span><br><br>

                        <b>4. 개인정보에 관한 민원서비스</b><br>
                        회사는 고객의 개인정보를 보호하고 개인정보와 관련한 불만을 처리하기 위하여 아래와 같이 관련 부서 및 개인정보관리책임자를 지정하고 있습니다.<br><br>

                        개인정보관리책임자 : 라돈닥터<br>
                        전화번호 : 02-554-3869<br>
                        이메일 : <a href="mailto:radon@candh.co.kr">radon@candh.co.kr</a><br><br>

                        회사의 서비스를 이용하시며 발생하는 모든 개인정보보호 관련 민원을 개인정보관리책임자 혹은 담당부서로 신고하실 수 있습니다.
                        회사는 이용자들의 신고사항에 대해 신속하게 충분한 답변을 드릴 것입니다.<br><br>

                        기타 개인정보침해에 대한 신고나 상담이 필요하신 경우에는 아래 기관에 문의하시기 바랍니다.<br><br>

                        1.개인정보침해신고센터 (www.1336.or.kr/ 국번없이 118)<br>
                        2.정보보호마크인증위원회 (www.eprivacy.or.kr/ 02-580-0533~4)<br>
                        3.대검찰청 인터넷범죄수사센터 (http://icic.sppo.go.kr/ 02-3480-3600)<br>
                        4.경찰청 사이버테러대응센터 (www.ctrc.go.kr/ 02-392-0330)
                    </div>
                </div>

                <div class="check_agree mt10">
                    <input type="checkbox" id="agree" name="agree" value="1" checked="checked">
                    <label for="agree" class="agree"> 개인정보처리방침에 동의해주세요.</label>
                </div>

                <h4 class="contact-title">기본정보</h4>
                <div class="contact-box table-responsive">
                    <table class="table form">
                        <tbody>
                            <tr>
                                <th scope="row">
                                    <span class="hide">(필수사항)</span>
                                    <label for="damdang">성함</label>
                                </th>
                                <td>
                                    <input type="text" required="required" id="damdang" name="manager">
                                </td>
                                <th scope="row">
                                    <span class="hide">(필수사항)</span>
                                    <label for="tell">연락처</label>
                                </th>
                                <td>
                                    <input type="text" id="tell" name="tel" required = "required">
                                </td>
                                <th scope="row">
                                    <span class="hide">(필수사항)</span>
                                    <label for="email1">이메일</label>
                                </th>
                                <td>
                                    <input type="text" name="email" id="email1" title="이메일 아이디 입력" required = "required">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <h4 class="contact-title">문의사항</h4>
                <textarea class="form contact-textarea" name="question" id="comment"></textarea>

                <div class="contact-button text-center mb20">
                    <button type="button" id="contactgo">문의메일 발송</button>
                    <button type="submit" style="display : none;"></button>
                </div>
            </form>
        </div>
    </section>
    <!-- Portfolio_END -->

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 p0">
                    <div class="foot_logo">
                        <img src="/assets/images/logo_w.png" title="로고" alt="로고">
                    </div>
                    <div class="foot_cont">
                        상호명 : C&H, Inc | 주소 : 서울특별시 강남구 강남대로 320, 황화빌딩 1505호<br>
                        대표전화 : (+82) 02-554-3869 | 팩스 : (+82) 02-556-0480 | 이메일 : radon@candh.co.kr
                        <span class="m_link">
                            <a href="http://www.newturntree.com/" target="_blank" title="뉴턴트리 홈페이지로 이동" alt="뉴턴트리 홈페이지로 이동">Newturn Tree</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Video</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Studio</a>
                        </span>
                    </div>
                    <div class="foot_contact">
                        <button onclick="location.href='/sub/contactus'" title="Contact Us로 이동" alt="Contact Us로 이동">
                            <i class="fal fa-paper-plane"></i>
                        </button>
                    </div>
                </div>
                <div class="col-xs-12 copyright">
                    <p>Copyright &copy; radon Dr. All rights reserved.</p>
                </div>
            </div><!-- row end -->
        </div><!-- Container_END -->
    </footer>

    <!-- Float-btn -->
    <div class="float-btn">
        <a href="/sub/qna">
            <img src="/assets/images/main/qna-icon-g.png" alt="">
        </a>
    </div>
    <!-- Float-btn_END-->

    <!-- Scroll to top -->
    <div class="scroll-up" title="TOP" alt="TOP">
        <a href="#contactus"><span class="glyphicon glyphicon-menu-up"></span></a>
    </div>
    <!-- Scroll to top end-->

    <!-- Javascript files -->
    <script src="/assets/lib/sticky/jquery.sticky.js"></script>
    <script src="/assets/lib/wow/wow.min.js"></script>
    <script src="/assets/lib/isotope/isotope.pkgd.min.js"></script>

    <script src="/assets/js/mail.js"></script>
    <script src="/assets/js/sub.js"></script>
</body>
</html>
