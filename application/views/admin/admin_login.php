<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>로그인 </title>

    <!-- Bootstrap -->
    <link href="/assets/template/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/assets/template/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="/assets/template/animate.css/animate.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="/assets/template/build/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form onsubmit="return false;">
              <h1>Login Form</h1>
              <div>
                <input type="text" class="form-control req log" name = "id" placeholder="Username" required="required" />
              </div>
              <div>
                <input type="password" class="form-control req log" name = "pass" placeholder="Password" required="required" />
              </div>
              <div>
                <a id = "logingo" class="btn btn-default submit">Log in</a>
                <button type = "submit" style = "display : none;"></button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <div>
                    <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
    <!-- jQuery -->
    <script src="/assets/template/jquery.min.js"></script>
    <script src="/assets/js/common.js"></script>
    <script src="/assets/js/admin.js"></script>
  </body>
</html>
