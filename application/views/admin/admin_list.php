<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
      </div>
    </div>

    <div class="clearfix"></div>
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h3><?=$name?></h3>
              <div style="display:block">
              <?if($insert) {?>
                  <a href="<?=$insert?>" style="display:inline-block;"><button type="button" class="btn btn-primary">등록하기</button></a>
              <?}?>
              <?if($search) {?>
                <p style="display:inline-block;"><?=$search[0]?></p><input type="text" id="search" data-type="<?=$search[1]?>">
              <?}?>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="x_content">

            <div class="table-responsive">
              <table class="table table-striped jambo_table bulk_action">
                <thead>
                  <tr class="headings">
                    <?foreach($col as $value) {?>
                      <th class="column-title"> <?=$value?> </th>
                    <?}?>
                  </tr>
                </thead>
                <tbody>
                <?for($i = 0; $i < count($list); $i++) {?>
                    <tr class="even pointer" data-idx = "<?=$list[$i]->idx?>">
                        <?foreach($list[$i] as $key => $value) {
                          if(strpos($key,'image') ==!  false) {
                             if(strpos($value,',') ==! false) {
                               $value = explode(',',$value)[0];
                             }?>
                            <td class=" "><img src="<?=SURL?>assets/uploads/<?=$value?>" style="width:80px;"></td>
                        <?} else {?>
                          <td class=" "><?=$value?></td>
                        <?}?>
                      <?}?>
                        <?=$adminbutton?>
                    </tr>
                <?}?>
                </tbody>

              </table>
            </div>
          </div>
        </div>
      </div>

  </div>
  <div style="margin-top: 20px; text-align: center;">
    <?echo $this->pagination->create_links();?>
  </div>
</div>
<!-- /page content -->
