<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
      </div>
    </div>

    <div class="clearfix"></div>
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h3><?=$name?></h3>
              <div style="display:block">
              <?if($insert) {?>
                  <a href="<?=$insert?>" style="display:inline-block;"><button type="button" class="btn btn-primary">등록하기</button></a>
              <?}?>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="x_content">

            <div class="table-responsive">
              <table id="datatable" class="table table-striped table-bordered">
                <thead>
                  <tr class="headings">
                    <?foreach($col as $value) {?>
                      <th class="column-title"> <?=$value?> </th>
                    <?}?>
                  </tr>
                </thead>
                <tbody>
                <?for($i = 0; $i < count($list); $i++) {?>
                    <tr class="even pointer" data-idx = "<?=$list[$i]->idx?>">
                        <?foreach($list[$i] as $key => $value) {
                          if($key != "idx") {
                            if(strpos($key,'image') ==!  false) {
                               if(strpos($value,',') ==! false) {
                                 $value = explode(',',$value)[0];
                               }?>
                              <td class=" "><img src="<?=SURL?>assets/uploads/<?=$value?>" style="width:80px;"></td>
                          <?} else {?>
                            <td class=" "><?=$value?></td>
                          <?}
                          }
                        }?>
                        <?=$adminbutton?>
                    </tr>
                <?}?>
                </tbody>

              </table>
            </div>
          </div>
        </div>
      </div>

  </div>

</div>
<!-- /page content -->

  <!-- footer content -->
    <footer>
      <div class="pull-right">
        Copyright © radon Dr. All rights reserved.
      </div>
      <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
    </div>
  </div>


    <!-- jQuery -->
    <script src="/assets/template/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/assets/template/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="/assets/js/common.js"></script>
    <script src="/assets/js/admin.js"></script>

    <!-- Datatables -->
    <script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="/assets/build/js/custom.min.js"></script>

  </body>
</html>
