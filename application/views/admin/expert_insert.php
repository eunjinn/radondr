<!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="clearfix"></div>

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_content">

                  <form class="form-horizontal form-label-left">
                    <span class="section"><?=$name?></span>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"> 제목 <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="name" class="form-control col-md-7 col-xs-12 req" name="expert_title" value="<?=$data->expert_title?>" required="required">
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image"> 내용 <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea id="expert_text" name="expert_text"><?=$data->expert_text?></textarea>
                      </div>
                    </div>


                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-md-offset-3">
                        <?if($data){ $id = "expertedit"; $text = "수정하기";} else { $id = "expertgo"; $text = "등록하기"; }?>
                        <button type="button" id="<?=$id?>" class="btn btn-success"><?=$text?></button>
                        <button type="submit" style="display:none;"></button>
                      </div>
                    </div>
                    <input type="hidden" id="idx" name="expert_idx" value="<?=$data->expert_idx?>">
                    <input type="hidden" name="admin_idx" value="<?=$_SESSION['idx']?>">
                    <input type="file" id="imagesUpload" style="display:none;">

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /page content -->

      <!-- footer content -->
        <footer>
          <div class="pull-right">
            Copyright © radon Dr. All rights reserved.
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
        </div>
      </div>


  <!-- jQuery -->
  <script src="/assets/template/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="/assets/template/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- Custom Theme Scripts -->
  <script src="/assets/js/common.js"></script>
  <script src="/assets/js/admin.js"></script>
  <script src="/assets/template/buildjs/custom.min.js"></script>

  <script src="/smarteditor/js/service/HuskyEZCreator.js" charset="utf-8"></script>
  <script>
  oEditors = [];
         nhn.husky.EZCreator.createInIFrame({
             oAppRef: oEditors,
             elPlaceHolder: "expert_text",
             sSkinURI: "/smarteditor/SmartEditor2Skin.html",
             fCreator: "createSEditor2"
         });
   $('#imagesUpload').change(function() {
      var json = uploadImage($(this));
      if (json) {
          if (json.return == 'true') {
              for (var variable in json.url) {
                  if (json.url.hasOwnProperty(variable)) {
                      if ($('textarea+iframe').length) {
                          var sHTML = "<img style='max-width:100%' src='/assets/uploads/"+json.url[variable]+"' />";
                          oEditors.getById['expert_text'].exec("PASTE_HTML", [sHTML]);
                      }
                  }
              }
              $(this).val('');
          }
      } else {
          alert('서버 에러');
      }
  });
  </script>

  </body>
</html>
