      <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>

            <div class="x_panel">
              <div class="x_title">
                <h3><?=$name?></h3>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <br>

                  <div class="form-group">
                    <label class="control-label col-md-3">글쓴이</label>
                    <div class="col-md-7">
                        <p class = "form-control col-md-7 col-xs-12"><?=$data['data']->qna_id?></p>
                    </div>

                    <label class="control-label col-md-3">내용</label>
                    <div class="col-md-7">
                        <textarea class = "form-control col-md-7 col-xs-12" readonly><?=$data['data']->qna_text?></textarea>
                    </div>

                    <label class="control-label col-md-3">답변</label>
                    <div class="col-md-7">
                        <textarea id = "qnaAnswer_text" class = "form-control col-md-7 col-xs-12"><?=$data['reply']->qna_text?></textarea>
                    </div>

                  </div>
                  <div class="ln_solid"></div>
                  <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                      <?if($data['reply']->qna_text){ $id = "qnaedit"; $text = "답변 수정";} else { $id = "qnago"; $text = "답변 등록"; }?>
                      <button type="button" id="<?=$id?>" class="btn btn-success"><?=$text?></button>
                      <input type="hidden" id="idx" value = "<?=$data['data']->qna_idx?>">
                    </div>
                  </div>

              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

      <!-- footer content -->
        <footer>
          <div class="pull-right">
            Copyright © radon Dr. All rights reserved.
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
        </div>
      </div>


        <!-- jQuery -->
        <script src="/assets/template/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="/assets/template/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="/assets/js/common.js"></script>
        <script src="/assets/js/admin.js"></script>
        <!-- Bootstrap -->
        <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="/vendors/fastclick/lib/fastclick.js"></script>
        <!-- iCheck -->
        <script src="/vendors/iCheck/icheck.min.js"></script>
        <!-- Datatables -->
        <script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
        <script src="/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
        <script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
        <script src="/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

        <!-- Custom Theme Scripts -->
        <script src="/assets/build/js/custom.min.js"></script>
      </body>
    </html>
