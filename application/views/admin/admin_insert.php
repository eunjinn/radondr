  <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">

                    <form class="form-horizontal form-label-left">
                      <span class="section"><?=$name?></span>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"> 샘플 이름 <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="name" class="form-control col-md-7 col-xs-12 req" name="name" value="<?=$data->list_name?>" required="required">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image"> 샘플 이미지 <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="file" id="image" class="form-control col-md-7 col-xs-12" required="required" multiple>
                          <?if($data->list_image != "") {
                              if(strpos($data->list_image,',') ==! false) {
                                  $images = explode(',',$data->list_image);
                                  for($i=0; $i<count($images); $i++) {?>
                                      <img src="<?=SURL?>assets/uploads/<?=$images[$i]?>" class="moreimages">
                                      <input type="hidden" name="image" class="form-control col-md-7 col-xs-12 req" value = "<?=$images[$i]?>">
                                  <?}?>
                             <?} else {?>
                                   <img src="<?=SURL?>assets/uploads/<?=$data->list_image?>" class="moreimages">
                                   <input type="hidden" name="image" class="form-control col-md-7 col-xs-12 req" value = "<?=$data->list_image?>">
                             <?}?>
                          <?}?>
                        </div>
                      </div>


                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <?if($data){ $id = "sampleedit"; $text = "수정하기";} else { $id = "samplego"; $text = "등록하기"; }?>
                          <button type="button" id="<?=$id?>" class="btn btn-success"><?=$text?></button>
                          <button type="submit" style="display:none;"></button>
                        </div>
                      </div>

                      <input type="hidden" id="idx" value="<?=$data->list_idx?>">

                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
