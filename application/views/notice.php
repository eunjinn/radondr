<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">

    <meta name="keywords" content="Radon Dr.">
    <meta name="description" content="Global Total Radon Solution">

    <link href="/assets/images/favicon/favicon.png" rel="shortcut icon" type="image/x-icon">
    <link href="/assets/images/favicon/favicon.png" rel="icon" type="image/x-icon">

    <title>Radon Dr.</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="/assets/css/global.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/notice.css" rel="stylesheet" type="text/css">
</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>
    <!-- Preloader_END -->

    <!-- Navigation -->
    <header>
        <nav class="navbar navbar-global navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="logo">
                    <a href="/">
                        <img src="/assets/images/logo_w.png" title="로고" alt="로고"  />
                    </a>
                </div>

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="custom-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="/sub/radondr">Radon Dr.</a>
                        </li>
                        <li>
                            <a href="/sub/solution">Solution</a>
                        </li>
                        <li>
                            <a href="/sub/review">Review</a>
                        </li>
                        <li>
                            <a href="/sub/notice">Notice</a>
                        </li>
                        <li>
                            <a href="/sub/contactus">Contact Us</a>
                        </li>
                        <li>
                            <a href="http://www.geomall.kr/" target="_blank">Shop</a>
                        </li>
                    </ul>
                </div>
            </div><!-- Container_END -->
        </nav>
    </header>
    <!-- Navigation_END -->

    <!-- Notice -->
    <section id="notice" class="wow fadeInUp">
        <div class="container-fluid">
            <div class="container">
                <div class="section-header">
                    <h3>Radon Dr. Notice</h3>
                </div>
            </div>
        </div>
        <div class="container-fluid p0">
            <div class="container mb80">
                <div class="search-bar">
                    <div class="col-lg-9 col-md-6 col-sm-4 col-xs-12">
                        <form>
                            <select id="select" class="form-select">
                                <option value="1" <?if($type == 1) { echo "selected"; }?>>내용</option>
                                <option value="2" <?if($type == 2) { echo "selected"; }?>>제목</option>
                                <option value="3" <?if($type == 3) { echo "selected"; }?>>내용+제목</option>
                            </select>
                        </form>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-8 col-xs-12">
                        <form class="form" role="form">
                            <div class="input-group" title="검색어를 입력해 주세요." alt="검색어를 입력해 주세요.">
                                <input class="form-control" type="text" id="word" placeholder="검색어를 입력해 주세요." name="searchText" value="<?=$word?>">
                                <div class="input-group-btn" title="검색" alt="검색">
                                    <button id="noticego" class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="vboard">
                    <div class="col-xs-12">
                        <div class="table-responsive col-xs-12 p0">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="width: 10%;">번호</th>
                                        <th style="width: 63%;">제&nbsp;목</th>
                                        <th style="width: 12%;">작성자</th>
                                        <th style="width: 15%;">등록일</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?foreach($list as $row) {?>
                                    <tr>
                                        <td><?=$row->num?></td>
                                        <td style="text-align: left;">
                                            <a href="/sub/notice_view/<?=$row->notice_idx?>">
                                                <p><?=$row->notice_title?></p>
                                            </a>
                                        </td>
                                        <td>
                                            <p><?=$row->admin_name?></p>
                                        </td>
                                        <?$arr = explode(' ',$row->notice_date);  $arrs = explode('-',$arr[0]); $date = implode('. ',$arrs);?>
                                        <td>
                                            <p><?=$date?></p>
                                        </td>
                                    </tr>
                                  <?}?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="vboard-paging">
                    <div class="col-xs-12">
                        <div class="dataTables_paginate paging_bootstrap_number" id="sample_1_paginate">
                            <?echo $this->pagination->create_links();?>
                        </div>
                    </div>
                </div>
            </div><!-- /.container -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- Notice_END -->

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 p0">
                    <div class="foot_logo">
                        <img src="/assets/images/logo_w.png" title="로고" alt="로고">
                    </div>
                    <div class="foot_cont">
                        상호명 : C&H, Inc | 주소 : 서울특별시 강남구 강남대로 320, 황화빌딩 1505호<br>
                        대표전화 : (+82) 02-554-3869 | 팩스 : (+82) 02-556-0480 | 이메일 : radon@candh.co.kr
                        <span class="m_link">
                            <a href="http://www.newturntree.com/" target="_blank" title="뉴턴트리 홈페이지로 이동" alt="뉴턴트리 홈페이지로 이동">Newturn Tree</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Video</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Studio</a>
                        </span>
                    </div>
                    <div class="foot_contact">
                        <button onclick="location.href='/sub/contactus'" title="Contact Us로 이동" alt="Contact Us로 이동">
                            <i class="fal fa-paper-plane"></i>
                        </button>
                    </div>
                </div>
                <div class="col-xs-12 copyright">
                    <p>Copyright &copy; Radon Dr. All rights reserved.</p>
                </div>
            </div><!-- row end -->
        </div><!-- Container_END -->
    </footer>

    <!-- Scroll to top -->
    <div class="scroll-up" title="TOP" alt="TOP">
        <a href="#notice"><span class="glyphicon glyphicon-menu-up"></span></a>
    </div>
    <!-- Scroll to top end-->

    <!-- Javascript files -->
    <script src="/assets/lib/sticky/jquery.sticky.js"></script>
    <script src="/assets/lib/wow/wow.min.js"></script>
    <script src="/assets/lib/isotope/isotope.pkgd.min.js"></script>
    <script src="/assets/lib/cbpViewModeSwitch.js"></script>
    <script src="/assets/lib/classie.js"></script>

    <script src="/assets/js/sub.js"></script>
    <script src="/assets/js/radondrdev.js"></script>
</body>
</html>
