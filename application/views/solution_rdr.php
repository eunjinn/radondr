<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">

    <meta name="keywords" content="radon Dr.">
    <meta name="description" content="Global Total Radon Solution">

    <link href="/assets/images/favicon/favicon.png" rel="shortcut icon" type="image/x-icon">
    <link href="/assets/images/favicon/favicon.png" rel="icon" type="image/x-icon">

    <title>radon Dr.</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

    <link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/lib/animate/animate.css" rel="stylesheet" type="text/css">

    <link href="/assets/css/global.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/sub.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/solutions.css" rel="stylesheet" type="text/css">
</head>
<body>

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>
    <!-- Preloader_END -->

    <!-- Navigation -->
    <header>
        <nav class="navbar navbar-global navbar-fixed-top" role="navigation">
            <div class="container p0">
                <div class="logo">
                    <a href="/">
                        <img src="/assets/images/logo_w.png" title="로고" alt="로고"  />
                    </a>
                </div>

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="custom-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="/sub/radondr">라돈닥터</a>
                        </li>
                        <li>
                            <a href="/sub/solution">솔루션</a>
                        </li>
                        <li>
                            <a href="/sub/review">저감사례</a>
                        </li>
                        <li>
                            <a href="/sub/news">뉴스</a>
                        </li>
                        <li>
                            <a href="/sub/qna">Q&A</a>
                        </li>
                        <li>
                            <a href="/sub/contactus">문의하기</a>
                        </li>
                        <li>
                            <a href="http://www.geomall.kr/" target="_blank">쇼핑몰</a>
                        </li>
                    </ul>
                </div>
            </div><!-- Container_END -->
        </nav>
    </header>
    <!-- Navigation_END -->

    <!-- Solution -->
    <section id="solution" class="wow fadeInUp">
        <div class="container">
            <div class="section-header">
                <p>라돈 저감 솔루션(Radon Mitigation Solution)</p>
                <h3>radon Dr. RDR</h3>
            </div>
            <div class="row solution">
                <div class="sol_1">
                    <div class="col-xs-12">
                        <p>
                            "이러한 점들을 고려하여 실내로 유입될 수 있는 라돈의 위험도를 평가하고, 그 위험도에 따른 적절한 저감 시설의 규모와 공법을 최소화 하여 적용함에 따라 불필요한 라돈 저감 시공 비용을 줄이고 라돈의 위협으로 부터 실내 환경의 안전을 확보하는 라돈 닥터만의 솔루션입니다. (토양의 라돈 위험도 측정시스템 및 그 측정 방법 :: 1014437940000)"
                        </p>
                        <p>
                            실내의 라돈은 대부분이 토양으로 부터(85~97%) 유입이 되기 때문에, 신규 건물 건축 시 토양으로부터의 라돈 유입을 미연에 방지하는 것은 실내 라돈 문제를 해결하는데 매우 중요 합니다. 토양 중 라돈 농도는 기반암에 따른 토양의 구성 성분에 따라 다르게 나타나며, 이러한 토양 라돈 가스는 토양의 수분 함량과 공기의 진공흡입도에 따라 실내로의 유입될 수 있는 정도의 차이를 발생시킵니다. 토양 중 수분함량이 높고 진공흡입도가 높을수록 토양 내 공기의 이동이 감소하고, 토양 수분함량과 진공흡입도가 낮을 수록 이동이 증가합니다.
                        </p>
                    </div>
                </div>
                <div class="sol_2 wow fadeInUp">
                    <div class="row m0 mb50">
                        <div class="step rdr">
                            <img src="/assets/images/solution/stethoscope.png" alt="">
                            <p>Ⅰ. 종합검사</p>
                            <p>
                                1) 건축부지 전역 토양 중 라돈 측정<br>
                                2) 건축부지 전역 토양 수분함량 측정<br>
                                3) 건축부지 전역 토양 진공흡입도 측정<br>
                                4) 라돈 투과율 산출
                            </p>
                        </div>
                        <div class="step rdr">
                            <img src="/assets/images/solution/report.png" alt="">
                            <p>Ⅱ. 진단</p>
                            <p>
                                1) Data Report<br>
                                2) 건축 형태, 변수 Report<br>
                                3) RDR index 산출
                            </p>
                        </div>
                        <div class="step rdr">
                            <img src="/assets/images/solution/tablets.png" alt="">
                            <p>Ⅲ. RDR 처방</p>
                            <p>
                                Level 1) 라돈 차단 시공 불필요<br>
                                Level 2) RDR membrane 시공<br>
                                Level 3) RDR mat 시공<br>
                                Level 4) RDR mat + 라돈팬 시공<br>
                                Level 5) RDR membrane + RDR mat + 라돈팬 시공
                            </p>
                        </div>
                        <div class="step rdr">
                            <img src="/assets/images/solution/medical-kit.png" alt="">
                            <p>Ⅳ. 정기검진</p>
                            <p>
                                Level 1) 매 2년간 정기적 검사<br>
                                Level 2) 매 2년간 정기적 검사<br>
                                Level 3) 매 1년간 정기적 검사<br>
                                Level 4) 매 1년간 정기적 검사<br>
                                Level 5) 지속적 모니터링
                            </p>
                        </div>
                    </div>
                </div>
                <div class="sol_3">
                    <button class="btn" onclick="location.href='/sub/solution'"><i class="fas fa-arrow-left"></i> 솔루션으로 돌아가기</button>
                </div>
            </div>
        </div>
    </section>
    <!-- Solution_END -->

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 p0">
                    <div class="foot_logo">
                        <img src="/assets/images/logo_w.png" title="로고" alt="로고">
                    </div>
                    <div class="foot_cont">
                        상호명 : C&H, Inc | 주소 : 서울특별시 강남구 강남대로 320, 황화빌딩 1505호<br>
                        대표전화 : (+82) 02-554-3869 | 팩스 : (+82) 02-556-0480 | 이메일 : radon@candh.co.kr
                        <span class="m_link">
                            <a href="http://www.newturntree.com/" target="_blank" title="뉴턴트리 홈페이지로 이동" alt="뉴턴트리 홈페이지로 이동">Newturn Tree</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Video</a> | <a href="#" title="현재 준비중" alt="현재 준비중">Newturn Tree Studio</a>
                        </span>
                    </div>
                    <div class="foot_contact">
                        <button onclick="location.href='./contactus.html'" title="Contact Us로 이동" alt="Contact Us로 이동">
                            <i class="fal fa-paper-plane"></i>
                        </button>
                    </div>
                </div>
                <div class="col-xs-12 copyright">
                    <p>Copyright &copy; radon Dr. All rights reserved.</p>
                </div>
            </div><!-- row end -->
        </div><!-- Container_END -->
    </footer>

    <!-- Float-btn -->
    <div class="float-btn">
        <a href="/sub/qna">
            <img src="/assets/images/main/qna-icon-g.png" alt="">
        </a>
    </div>
    <!-- Float-btn_END-->

    <!-- Scroll to top -->
    <div class="scroll-up" title="TOP" alt="TOP">
        <a href="#solution"><span class="glyphicon glyphicon-menu-up"></span></a>
    </div>
    <!-- Scroll to top end-->

    <!-- Javascript files -->
    <script src="/assets/lib/wow/wow.min.js"></script>
    <script src="/assets/lib/isotope/isotope.pkgd.min.js"></script>

    <script src="/assets/js/sub.js"></script>
</body>
</html>
